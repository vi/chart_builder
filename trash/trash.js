// trash.js
// =============================================================================

	/**
	 * DataObject - the database object's base class
	 */
	var DataObject = Observable.extend({
		
		/**
		 * All the methods are defined within the constructor because they have 
		 * to deal with the private _data store.
		 * @constructor
		 */ 
		init : function DataObject( data ) 
		{
			// First init the Observable
			this._super();
			
			/**
			 * The data is stored here.
			 * @type Object
			 * @private
			 */
			var _data = $.extend({}, data);
			
			/**
			 * Get a proprty by it's name.
			 *
			 * @param {string} name The name of the property to set.
			 * @returns The value of the named property ( or undefined if there is 
			 * no such property).
			 */
			this.get = function( name ) {
				return _data.hasOwnProperty(name) ? _data[name] : undefined;
			};
			
			/**
			 * Sets a property at the "_data" store.
			 * First it fires an "beforePropertySet" event. The "data" property of 
			 * that event will be an object that contains:
			 *    name     - the name of the proprty that is about to be set 
			 *    oldValue - the current value (or undefined if no such property)
			 *    newValue - the new value that should be set
			 * If any of the event listeners bound to the "beforePropertySet" event 
			 * returns false or calls the "stop" or "preventDefault" methods of the 
			 * event the action will be discarded.
			 * Similarry an "afterPropertySet" event (having the same data 
			 * properties) will be dispatched after the 
			 * property was set, but off course it cannot be canceled.
			 *
			 * @param {string} name The name of the property to set 
			 * @param {any} value The value to set.
			 * @param {Boolean} silent If TRUE all the events will be skipped
			 * @returns {DataObject} Returns this instance.
			 */
			this.set = function( name, value, silent ) {
				if (!!silent) {
					_data[name] = value;
				}
				else {
					var old = _data[name];
					var evt = new Event("beforePropertySet", {
						name     : name, 
						oldValue : old,
						newValue : value
					});
					if (this.dispatchEvent(evt) && !evt.isDefaultPrevented()) {
						_data[name] = value;
						this.dispatchEvent("afterPropertySet", {
							name     : name, 
							oldValue : old,
							newValue : value
						});
					}
					old= evt = null;
				}
				return this;
			};
			
			/**
			 * Deletes a property. If the property does not exist nothing happens. 
			 * Otherwise an "beforePropertyDelete" event will be dispatched first.
			 * The event's "data" object will contain "name" and "value" properties 
			 * matching the name and the value of the property that is about to be 
			 * deleted. The action can be canceled by returning false from any event 
			 * handler or calling the "stop" or "preventDefault" methods of the vent.
			 * After the property was deleted an "afterPropertyDelete" event will be 
			 * dispatched, but it cannot be canceled.
			 *
			 * @param {string} name The name of the property to set 
			 * @param {Boolean} silent If TRUE all the events will be skipped
			 * @returns {DataObject} Returns this instance.
			 */
			this.unset = function( name, silent ) {
				if ( _data.hasOwnProperty(name) ) {
					if ( !!silent ) {
						delete _data[name];
					}
					else {
						var old = _data[name];
						var evt = new Event("beforePropertyDelete", {
							name  : name, 
							value : old
						});
						if (this.dispatchEvent(evt) && !evt.isDefaultPrevented()) {
							delete _data[name];
							this.dispatchEvent("afterPropertyDelete", {
								name     : name, 
								oldValue : old
							});
						}
						old= evt = null;
					}
				}
				return this;
			};
			
			/**
			 * Helper method to iterate over the data properties.
			 *
			 * @param {function} callback The callback function to be called on each 
			 * property with arguments:
			 *     1. "name"
			 *     2. "value"
			 * @returns {DataObject} Returns this instance.
			 */
			this.each = function( callback ) {
				return $.each( _data, callback );
			};
			
			/**
			 * When converted to String the object displays the JSON representation 
			 * of it's _data store.
			 * @returns String
			 */
			this.toString = function() {
				return JSON.stringify(_data);
			};
			
			/**
			 * The destructor shpuld purge any event listeners
			 */
			this.unInit = function() {
				this._super();
				_data = null;
			};
		}
	}, "DataObject");
	
	/**
	 * @class This is a custom implementation of the HTMLCommandElement
	 * interface in HTML5 specification.
	 * @see <a href="http://www.whatwg.org/specs/web-apps/current-work/multipage/
	 * interactive-elements.html#htmlcommandelement" target="_blank">
	 * htmlcommandelement</a>
	 * @param {object} options The options for this command. Those will augment the 
	 * object and override the defaults from the prototype.
	 */
	var Command = Class.extend({
		
		/**
		 * @constructor
		 * @param {object} options The options for this command. Those will 
		 * augment the object and override the defaults from the prototype.
		 */
		init : function( options ) {
			$.extend(this, options);
		},
		
		/**
		 * The command's label is a multi-purpose string (translatable key)
		 * @type {string}
		 */
		label : "Unknown label",

		/**
		 * The command's title is a string (translatable key), displayed as a tooltip
		 * on some widgets.
		 * @type {string}
		 */
		title : null,

		/**
		 * This can be null, meaning the command has no icon, string - a static path 
		 * to the icon file, 
		 * @type {string|null}
		 */
		icon : null,

		/**
		 * Can be one of:
		 * Command  - a normal command with an associated action.
		 * Checkbox - a state or option that can be toggled.
		 * Radio    - a selection of one item from a list of items.
		 * @type {string}
		 */
		state : "Command",

		/**
		 * This is ONLY used if the state is Radio
		 * @type {string}
		 */
		radiogroup : null,

		/**
		 * Updating a command means that all it's subscribers (identified by 
		 * data-command attribute) and firing a "commandupdate" event on them. Then 
		 * each of these subscriber should deside what to do (enable/disable UI 
		 * elements, display different values etc)
		 * @returns {Command} Returns this command object
		 */
		update : function() {
			jQuery('[data-command="' + this.id + '"]').trigger("commandupdate", [this]);
		},

		/**
		 * This will be called by the application to determine if the command is 
		 * available on the current context.
		 * @returns {boolean}
		 */ 
		isAvailable : function() { 
			return true; 
		},

		/**
		 * This will be called by the application to determine if the command is 
		 * enabled in the current context.
		 * @returns {boolean}
		 */ 
		isEnabled : function() { 
			return true; 
		},

		/**
		 * This is ONLY used if the state is Checkbox or Radio
		 * @returns {boolean}
		 */
		isChecked : function() {
			return false;
		},

		/**
		 * This will be called by the application to determine what is the command's 
		 * value. Not all commands have a value, but those who have shpuld implement 
		 * this method.
		 * @returns {*} Anything can be expected
		 */ 
		getValue : function() { 
			return undefined; 
		},

		/**
		 * The main method of the command object (the one who does something :-))
		 * @returns {*} Anything can be expected
		 */ 
		execute : function() { 
			// do nothing by default :-)
		}
	}, "Command");


// TEST ////////////////////////////////////////////////////////////////////////
(function($) {
    
    function DataGrid( container, options ) {
        
        this.container = $(container).empty();
        
        this.widget = $(
             '<div class="data-grid">'
            +    '<div class="data-grid-container">'
            +       '<div class="data-grid-header"/>'
            +       '<div class="data-grid-body-container">'
            +           '<div class="data-grid-body"/>'
            +           '<div class="data-grid-scroll-y"><div class="scroller"/></div>'
            +       '</div>'
            +       '<div class="data-grid-scroll-x"><div class="scroller"/></div>'
            +    '</div>'
            +'</div>'
        ).appendTo(this.container);
        
        if (options.width) {
            this.setWidth(options.width);
        }
        
        if (options.height) {
            this.setHeight(options.height);
        }
        
        this.data = {
            cols : [{ label : "" }],
            rows : [["&nbsp;"]]
        };
        
        this.build(options.data);
    }
    
    DataGrid.prototype.setWidth = function(w) {
        this.widget.css("width", w);
    };
    
    DataGrid.prototype.setHeight = function(h) {
        this.widget.css("height", h);
    };
    
    DataGrid.prototype.build = function(data) {// console.log(this, arguments);
        
        this.data = $.extend(true, this.data, data);
        
        var colCount = this.data.cols.length,
            colIndex = -1,
            rowCownt = this.data.rows.length,
            colIndex = -1;
        
        var header = $('<table width="100%"><tr><th/></tr></table>');
        var body   = $('<table width="100%"><tbody><tr><td colspan="' + colCount + '"/></tr></tbody></table>');
        
        // header
        var tr = $("tr", header);
        $.each(this.data.cols, function(i, col) {
            tr.append(
                $('<th/>').text(col.label)
            );
        });
        $("th:first", tr).remove();
        $(".data-grid-header", this.container).empty().append(header);
        
        
        // body
        var tbody = $("tbody", body);
        $.each(this.data.rows, function(rowIndex, cells) {
            var row = $('<tr/>').appendTo(tbody);
            for (var i = 0; i < colCount; i++) {
                var cell = $('<td/>').html(cells[i] || "&nbsp;");
                cell.appendTo(row);
                
                //if (rowIndex === 0) {
                //    cell[0].width = $("th:eq(" + i + ")", header)[0].offsetWidth;
                //}
            }
        });
        $("tr:first", tbody).remove();
        $(".data-grid-body", this.container).empty().append(body);
        
    };
    
    $.fn.dataGrid = function(options) {
        
        var isFunctionCall = typeof options == "string";
        
        if (isFunctionCall) {
            var result, args = Array.prototype.slice.call(arguments, 1);
            this.each(function() {
                var instance = $(this).data("DataGrid");
                if(instance && $.isFunction(instance[options])) {
                    var result = instance[options].apply(instance, args);
                    if (result !== undefined) {
                        return false;// break
                    }
                }
            });
            return result === undefined ? this : result;
        }
        else {
            var cfg = $.extend({
               data : {} 
            }, options);
            
            this.each(function() {
                var instance = $(this).data("DataGrid");
                if(!instance) {
                    instance = new DataGrid(this, cfg);
                    $(this).data("DataGrid", instance);
                }
            });
        }
        
        return this;
    };
    
})(jQuery);

////////////////////////////////////////////////////////////////////////////
	$.embedEditor = function(cfg) {
		//{
		//  path          : "core/zingcharts/editors/reusable/text.html"
		//	target        : "body",
		//	eventType     : "settingsChange",
		//	props         : [],
		//	onChange      : null,
		//  onLoad        : null,
		//  initialData   : null,
		//  initArguments : []
		//}
		
		var DATA = {};
		
		if ( cfg.initialData ) {
			$.extend(DATA, cfg.initialData);
		}
		
		$.loadHTML(cfg.target, cfg.path).done(function() {
			
			$(cfg.target).off(cfg.eventType).on(cfg.eventType, function(e, data) {
				if ( e.target == $(cfg.target)[0] ) {
					e.stopPropagation();
					if ( $.extendByTemplate(data, DATA, cfg.props) ) {
						if ($.isFunction(cfg.onChange)) {//console.log("_changed", data, DATA)
							cfg.onChange(DATA, cfg.props);
						}
					}
				}
			});
			
			if ( cfg.initArguments ) {
				$(cfg.target).triggerHandler("initUI", initArguments);
			}
			
			if ($.isFunction(cfg.onLoad)) {
				cfg.onLoad();
			}
		});
	};
	
	$.EmbededEditor = function(container, dataProperties, elements) {
		
		var STATE        = {}; 
		var DATA_SOURCE  = {}; 
		var TRANSACTIONS = 0;
		
		this.container = container;
		
		this.dataProperties = $.extend([], dataProperties);
		
		// name -> defaultValue
		this.elements = $.extend({}, elements);
		
		this.beginTransaction = function() {
			TRANSACTIONS++;
		};
		
		this.endTransaction = function() {
			TRANSACTIONS = Math.max(--TRANSACTIONS, 0);
		};
		
		this.submit = function() {
			if ( TRANSACTIONS < 1 ) {
				
				$.each(this.dataProperties, function(i, name) {
					if (STATE.hasOwnProperty(name) && STATE[name] != "SKIP_THIS_PROPERTY") {
						DATA_SOURCE[name] = SATE[name];
					}
					else {
						if (DATA_SOURCE.hasOwnProperty(name)) {
							delete DATA_SOURCE[name];
						}
					}
				});
				
				this.onSubmit();
			}
		};
		
		this.reset = function() {
			var self = this;
			this.beginTransaction();
			$.each(this.elements, function(name, defaultValue) {
				var elem = self.getElement(name);
				if (elem.length) {
					if (elem.is(":checkbox, :radio")) {
						if (elem[0].checked !== !!defaultValue) {
							elem[0].checked = !!defaultValue;
							elem.triggerHandler("click");
						}
					}
					else {
						if (elem.val() !== defaultValue) {
							elem.val(defaultValue).triggerHandler("change");
						}
					}
				}
				elem = null;
			});
			this.endTransaction();
			self = null;
		};
		
		this.getState = function(state) {
			return STATE;
		};
		
		this.setState = function(state) {
			STATE = $.extend({}, state);
			this.submit();
		};
		
		this.set = function(name, value) {
			STATE[name] = value;
			return this;
		};
		
		this.get = function(name) {
			return STATE[name];
		};
		
		this.mapTo = function( obj ) {
			DATA_SOURCE = obj;
		};
		
		this.getElement = function(name) {
			return $('[name="' + name + '"]', this.container);
		};
		
		this.find = function(selector) {
			return $(selector, this.container);
		};
		
		this.onSubmit = $.noop;
		
		this.onReset  = $.noop;
		
		this.init = function() {}
	};