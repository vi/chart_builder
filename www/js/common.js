/*global window, jQuery, zingchart, Class, console*/
(function($) {
	
	"use strict";
	
	window.SKIP_THIS_PROPERTY = "SKIP_THIS_PROPERTY";
	
	$.floatVal = function(x, defaultValue) {
		x = parseFloat(x);
		if ( isNaN(x) ) {
			x = arguments.length < 2 ? 0 : defaultValue;
		}
		return x;
	};
	
	$.intVal = function(x, defaultValue) {
		x = parseInt(x, 10);
		if ( isNaN(x) ) {
			x = arguments.length < 2 ? 0 : defaultValue;
		}
		return x;
	};
	
	$.uid = function(prefix) {
		var id, n = 1;
		do {
			id = (prefix || "uid") + n++;
		} while (document.getElementById(id));
		return id;
	};
	
	$.loadCSS = function(css) {
		if ( !$('link[href="' + css + '"]').length ) {
			$('<link href="' + css + '" type="text/css" rel="stylesheet" media="all" />').appendTo("head");
		}
	};
	
	$.loadScript = function(path, callback) {
		var cur = $('script[src*="' + path + '"]');
		if ( !cur.length ) {
			var s = document.createElement("script");
			s.type = "text/javascript";
			s.async = true;
			s.src  = path;
			s.onload = callback;
			$("head")[0].appendChild(s);
		}
		else {
			if ( $.isFunction(callback) ) {
				callback.call(cur[0]);
			}
		}
	};
	
	$.evalWithEnvironment = function( script, env ) {
		try {
			eval("(function(ENV) {" + script + "})(env);");
		} catch (ex) {
			console.error([
				"Error evaluating script:",
				"Message: " + ex.message,
				"Script:\n" + $.elipsis(script, 500)
			].join("\n"));
		}
	};
	
	$.loadHTML = function(container, path) {
		
		var $container = $(container);
		var env = { container: $container[0] };
		
		var req = $.ajax({ url : path, dataType : "text" });
		
		req.done(function(html) {
			
			// Remove the scripts now (evaluate them in scope later)
			var scripts = [];
			html = html.replace(
				/<script\b[^>]*>([\s\S]*?)<\/script>/gi, 
				function(all, txt) {
					scripts.push(txt);
					return "";
				}
			);
			
			$container.empty().append(html);
			
			$container.trigger("newContent");
			
			$.each(scripts, function(i, s) {
				$.evalWithEnvironment( s, env );
			});
			
		});
		
		return req;
	};
	
	$.preloadImages = function() {
		// Preload the images that are passed as arguments and then delete the 
		// Image objects to free up that temp. memmory
		var cache = {}, l = arguments.length;
		$.each(arguments, function(i, x) {
			cache[x] = new Image();
			cache[x].onload = cache[x].onerror = function() {
				delete cache[x];
				if (--l <= 0) {
					cache = null;
				}
			};
			cache[x].src = x;
		});
	};
	
	$.areSameByValue = function(a, b) {
		if (a && typeof a == "object" && b && typeof b == "object") {
			return JSON.stringify(a) == JSON.stringify(b);
		}
		return a === b;
	};
	
	// JSON Functions 
	// =========================================================================
	
	/**
	 * @param {Object} obj The object to loop
	 * @param {String} path The path to set or get
	 * @param {any} value The value to set
	 * @param {function} filterFn (optionsl) The function to filter each path. 
	 *        The filterFn can return a modified value or return undefined to 
	 *        exclude the property from setting...
	 * @returns {any} In GET mode returns the value contained at the desired 
	 *                path or undefined if that path was not found.
	 *                In SET mode returns boolean which is TRUE ONLY is the path 
	 *                was set to a value different then it's prvious value.
	 */
	$.JSONPath = function( obj, path, value, filterFn ) {
		
		var cur = obj, 
			segments = path.replace(/\[['"]?([^\]]+)['"]?\]/g, ".$1").split("."),
			l = segments.length,
			name,
			curPath = [];
		
	    for ( var i = 0; i < l; i++ ) {
			curPath[i] = name = segments[i];
			if ( i == l - 1 ) { // last
				
				// Called to GET
				if ( arguments.length == 2 ) {
					return cur[name]; // can return undefined here
				}
				
				// Called to SET
				
				if (filterFn) {
					value = filterFn(obj, curPath.join("."), value);
				}
				
				if (value !== undefined) {
					//if (!$.areSameByValue(value, cur[name])) {
						cur[name] = value;
						return true;
					//}
				}
				return false;
			}
			else {
				if (!cur.hasOwnProperty(name)) {
					if ( arguments.length == 2 ) {
						return undefined;
					}
					cur[name] = isNaN(parseFloat(name)) || "" + parseFloat(name) !== "" + name ? {} : [];
				}
				cur = cur[name];
			}
		}
	};
	
	$.JsonModify = function(path, props, DATA) {
		$.each(props, function(i, prop) {
			if (DATA.hasOwnProperty(prop)) {
				$.JSONPath(data, prop, DATA[prop]);
			}
			else {
				$.JSONDelete(data, prop);
			}
		});
	};
	
	$.JSONDelete = function( obj, path ) {
		var cur = obj, 
			segments = path.replace(/\[['"]?([^\]]+)['"]?\]/g, ".$1").split("."),
			l = segments.length,
			name;
			
		for ( var i = 0; i < l; i++ ) {
			name = segments[i];
			if ( i == l - 1 ) { // last
				if ( cur.hasOwnProperty(name) ) {
					delete cur[name];
					return true;
				}
			}
			else {
				if (!cur.hasOwnProperty(name)) {
					return false;
				}
			}
			cur = cur[name];
		}
	};
	
	$.JsonBlackList = function( obj, list ) {
		var _list  = $.makeMap(list);
		
		function filter(o) {
			var newObj = $.isArray(o) ? [] : {};
			
			$.each(o, function(key, value) {
				if (value in _list) {
					return true; // continue
				}
				
				if ( value && typeof value == "object" ) {
					newObj[key] = filter(value);
				}
				else {
					newObj[key] = value;
				}
			});
			return newObj;
		}
		
		return filter(obj);
	};
	
	$.JsonCleanupEmptyObjectProperies = function( obj ) {
		function filter(o) {
			var newObj = $.isArray(o) ? [] : {};
			
			$.each(o, function(key, value) {
				if ( value && typeof value == "object" ) {
					newObj[key] = filter(value);
					if ($.isEmptyObject(newObj[key])) {
						delete newObj[key]; // continue
					}
				}
				else {
					newObj[key] = value;
				}
			});
			return newObj;
		}
		
		return filter(obj);
	};
	
	// =========================================================================
    // Path Functions
    // =========================================================================
	$.dirName = function(path) {
	    return path.replace(/(.*?)([\\\/]?[^\\\/]*)/, "$1");
	};
	
	$.baseName = function(path) {
        return path.replace(/.*?[\\\/]?([^\\\/]*)/, "$1");
    };
    
	// =========================================================================
	// String Functions
	// =========================================================================
	
	function htmlEncode(str) {
		return str  .replace(/&amp;/g, '&')
					.replace(/&/g, '&amp;')
					.replace(/</g, '&lt;')
					.replace(/>/g, '&gt;')
					.replace(/"/g, '&quot;');
	}
	
	function unQuote(theStr) {
	  if (theStr.length > 1) {
		if (theStr.substring(0,1) == "'") {
		  return theStr.substring(1,theStr.length-1).replace(/\\'/gi,"'");
		}
		if (theStr.substring(0,1) == '"') {
		  return theStr.substring(1,theStr.length-1).replace(/\\"/gi,'"');
		}    
	  }
	  return theStr;
	}

	function quote(str, useSingleQuotes) {
	  var _escapeable;
	  var quot = useSingleQuotes ? "'" : '"';
	  var tokenMap = {};
	  var _meta = {
		'\b': '\\b',
		'\t': '\\t',
		'\n': '\\n',
		'\f': '\\f',
		'\r': '\\r',
		'\\': '\\\\'
	  };
		
	  if (useSingleQuotes) {
		_meta["'"] = "\\'";
		_escapeable = /['\\\x00-\x1f\x7f-\x9f]/g;
	  } else {
		_meta['"'] = '\\"';
		_escapeable = /["\\\x00-\x1f\x7f-\x9f]/g;
	  }
		
	  function doQuote(str) {
		if (str.match(_escapeable)) {
		  return quot + str.replace(_escapeable, function(a) {
			var c = _meta[a];
			if (typeof c === 'string') {
				return c;
			}
			c = a.charCodeAt();
			return '\\u00' + Math.floor(c / 16).toString(16) + (c % 16).toString(16);
		  }) + quot;
		}
		return quot + str + quot;
	  }
	  
	  return doQuote(String(str));
	}
	
	$.sprintf = function(str) {
		var params = Array.prototype.slice.call(arguments, 1), i = 0;
		return String(str).replace(/(%[\w%])/gi, function(all, token) {
			if (token == "%%") {
				return "%";
			}
			if (i >= params.length) {
				return "";
			}
			
			var param = params[i];
			i++;
			token = token.charAt(1);
			switch (token) {
				case "i":
					return $.intVal(param);
				case "f":
					return $.floatVal(param);
				case "b":
					return !!param;
				case "s":
					return String(param);
				case "q":
					return quote(String(param));
				case "o":
					return prettyPrint(param, "\t", false, true, false);
				case "H":
					return prettyPrint(param, "\t", false, true, true);
				case "a":
					if (param && typeof param == "object") {
						return prettyPrint(param, "\t", false, true, false);
					}
					return param;
				case "A":
					if (param && typeof param == "object") {
						return prettyPrint(param, "\t", false, true, true);
					}
					return param;
				default:
					return param;
			}
		});
		
	}
	
	$.elipsis = function(str, len) {
        var l = str.length;
        len = len || 100;
        if (l > len - 3) {
            str = str.substring(0, len - 3) + "...";
        }
        return str;
    };
	
	function isScalarArray(x) {
		if (!$.isArray(x)) {
			return false;
		}
		var l = x.length;
		for ( var i = 0; i < l; i++ ) {
			if ( x[i] && typeof x[i] == "object" ) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param {Object} obj              - Object (or Array) to loop.
	 * @param {mixed} indentArg         - If and How to indent. The behavior depends on the
	 *                                    type of this argument and its value:
	 *                                    #indentArg is String - this string will be repeated
	 *                                                           and used for indentation
	 *                                                           (usually " " or "\t").
	 *                                    #indentArg is Number - a single whitespace character
	 *                                                           will be the indent string,
	 *                                                           and this is its multiplier
	 *                                                           (4 means "indent with 4 spaces").
	 *                                    #indentArg is Other (no string or number) - no indentation
	 *                                                           will be used (and no line terminators)
	 *                                                           ; compressed output
	 * @param {Boolean} useSingleQuotes - If TRUE, single quotes will be used. Defaults to FALSE.
	 * @param {Boolean} quoteLabels     - By default, labels (object's property names) are quoted
	 *                                    (respecting the useSingleQuotes argument) only if needed
	 *                                    (if they contain invalid varname chars). If quoteLabels
	 *                                    is set to TRUE, they will allways be quoted (valid JSON).
	 * @param {Boolean} html            - If TRUE the generated string will be an HTML snippet and 
	 *                                    any tokens will be wrapped in SPANs and B tags that can 
	 *                                    be colored useing CSS...
	 * @return {String} The formated string representation of the "obj" object.
	 * @use quote
	 */
	function prettyPrint(obj, indentArg, useSingleQuotes, quoteLabels, html) {
	  var _indentString = "";
	  var _indentCount  = 0;
	  var _indent       = 0;
	  var _eol          = "";
	  var _quoteLabels  = !!quoteLabels;
	  
	  if (typeof indentArg == "string") {
		_indentString = indentArg;
		_indentCount  = 1;
		_eol          = "\n";
	  } else if (typeof indentArg == "number") {
		_indentCount  = parseInt(indentArg, 10);
		_indentString = " ";
		_eol          = "\n";
	  } 
	  
	  function getIndentString() {
		var str = "";
		for (var i = 0; i < _indentCount*_indent; i++) {
		  str += _indentString;
		}
		return str;
	  }
	  
	  function quoteLabel(label) {
		
		if (_quoteLabels) {
		  return quote(label, useSingleQuotes);
		} 
		
		if ((/[^a-zA-Z0-9_\$]/).test(label)) {
		  return quote(label, useSingleQuotes);
		}
		
		return label;
	  }
	  
	  function loop(o) {
		var tmp = [],
			
			// note that checking instanceof or constructor will fail
			// the object is from other orign (frame)
			inArray = o instanceof Array || typeof o.push == "function",
			 
			current = "";// = inArray && isScalarArray(o) ? "" : getIndentString();
			
		_indent++;
		for (var x in o) {
		  
		  //current = getIndentString();
		  
		  current = inArray && isScalarArray(o) ? "" : getIndentString();
		  
		  if (!inArray) {
			current += html ? 
			'<span class="json-label">' + htmlEncode(quoteLabel(x)) + '</span><b>:</b> ' + (_eol ? " " : ""): 
			quoteLabel(x) + ':' + (_eol ? " " : "");
		  }
		  if (o[x] && typeof o[x] == 'object') {
			current += loop(o[x]);  
		  } else if (typeof o[x] == 'string') {
			current += html ? 
				'<span class="string">' + quote(htmlEncode(o[x]), useSingleQuotes) + '</span>' :
				quote(o[x], useSingleQuotes);
		  } else {
			current += html ? 
				'<span class="' + (typeof o[x]) + '">' + htmlEncode(String(o[x])) + '</span>' :
				String(o[x]);
		  }
		  tmp.push(current);
		}
		_indent--;
		
		// Display those on single row
		if (inArray && isScalarArray(o)) {
			tmp = tmp.join(html ? "<b>,</b> " : ", ");
			tmp = html ? '<b>[</b>' + tmp + '<b>]</b>' : '[' + tmp + ']';
		}

		// join with \n + indent
		else {
			tmp = tmp.join(html ? '<b>,</b>' + _eol : ',' + _eol);
			if (!tmp) {
				tmp = html ? '<b>' + (inArray ? '[]' : '{}') + '</b>' :
					(inArray ? '[]' : '{}');
			}
			else {
				if (inArray) {
					tmp = html ? ('<b>[</b>' + _eol + tmp + _eol + getIndentString() + '<b>]</b>') : 
						'[' + _eol + tmp + _eol + getIndentString() + ']';
				}
				else {
					tmp = html ? ('<b>{</b>' + _eol + tmp + _eol + getIndentString() + '<b>}</b>') : 
						('{' + (_eol + tmp + _eol + getIndentString()) + '}');
				}
			}		
		}
		
		return tmp;
	  }
	  
	  return loop(obj);
	}
	
	$.unQuote     = unQuote;
	$.quote       = quote;
	$.prettyPrint = prettyPrint;
	
	
	$.extend($.expr[':'], {  
		bound: function (el) {  
			return !!$.data(el, "events");  
		}  
	});
	
	$.getEventBindings = function(context) {
		return $(":bound", context || document).map(function(i, el) {
			return $.data(el, "events");
		});
	}; 
	
	$.fn.enable = function() {
		return this.each(function() {
			var origTitle = $(this).data("origTitle");
			if (origTitle) {
				$(this).prop("title", origTitle);
			}
			else {
				$(this).removeProp("title").removeAttr("title");
			}
			
			$(this).removeClass("disabled").removeProp("disabled")
				.find("input, select, button, a, textarea")
				.removeClass("disabled")
				.removeProp("disabled")
				.removeAttr("disabled");
		});
	};
	
	$.fn.disable = function() {
		return this.each(function() {
			var origTitle = $(this).data("origTitle");
			if (origTitle !== false) {
				origTitle = $(this).attr("title") || false;
				$(this).data("origTitle", origTitle);
			}
			
			
			$(this).addClass("disabled").prop("disabled", true)
				.find("input, select, button, a, textarea")
				.addClass("disabled").prop("disabled", true).attr("disabled", "disabled");
		});
	};
	
	// Ajax accordions
	// =========================================================================
	$("dt[data-url]").die("click.accordionPannel").live({
	    "click.accordionPannel": function(e) {
    		var dt    = $(this),
    			panel = dt.next("dd"),
    			keep  = dt.closest("dl").hasClass("keep-state");
    		
    		function hidePannel(p) {
    			$(p).prev("dt").removeClass("expanded");
    			$(p).css({ overflow : "hidden" }).animate({height:0}, 400, "swing", function() {
					$(this).trigger("hide");
    				if (!keep) {
    					$(this).empty().remove();
    				}
    				$(this).removeClass("expanded").addClass("collapsed");
    			});
    		}
    		
    		function showPannel(p) {
    			if ($(":animated", p).length) {
    				setTimeout(function() { showPannel(p); }, 100);
    				return;
    			}
    			
    			dt.closest("dl").find("dd.expanded").each(function() {
    				hidePannel(this);
    			});
    			
    			$(p).prev("dt").addClass("expanded");
    			$(p).animate({ height: p.scrollHeight }, 400, "swing", function() {
    				$(p)
    				.removeClass("collapsed")
    				.addClass("expanded")
    				.css({
    					height: "auto",
    					overflow: "visible"
    				})
					.trigger("show");
    			})
    		}
    		
    		// Create new pannel
    		if (!panel.length) {
    			var url = this.getAttribute("data-url");
    			if (!!url) {
    				dt.addClass("expanded loading");
    				panel = $('<dd class="collapsed">').insertAfter(this);
    				panel.on("ready.accordionPannel", function(e) {
    					if ( e.target === panel[0] ) {
    						showPannel(this);
    						dt.removeClass("loading"); 
    					}
    				});				
    				$.loadHTML(panel, url);
    			}
    			return;
    		}
    		
    		// collapse
    		if (panel.is(".expanded")) {
    			hidePannel(panel[0]);
    		}
    		
    		// expand
    		else {
    			showPannel(panel[0]);
    			dt.addClass("expanded");
    		}
    	},
    	"selectstart.accordionPannel mousedown.accordionPannel" : function(e) {
    	    e.preventDefault();
    	}
	});
	
	// Ajax tabs
	// =========================================================================
	(function() {
		/** 
		 * This works with html structure like:
		 * .tabs-widget
		 *     .tabs
		 *       .tab
		 *       .tab
		 *       ...
		 *  .pannels
		 *       .pannel
		 *       .pannel
		 *       ...
		 */
		$(".tabs .tab").live({
			"click" : function() {
				
				var $tab      = $(this).addClass("loading"),
					allTabs   = $tab.closest(".tabs").find(".tab"),
					url       = $tab.attr("data-url"),
					content   = $tab.closest(".tabs-widget").find(".pannels"),
					curHeight = content.height(),
					newPannel = $tab.data("panel");
					
				if (!this.hasAttribute("tabindex")) {
					$tab.attr("tabindex", "-1");
				}
				
				$tab.trigger("focus");
				
				content.css({
					overflow: "hidden",
					height: curHeight
				});
						
				function swap() {
					if ($(":animated", newPannel).length) {
						setTimeout(swap, 100);
						return;
					}
					
					////////////////////////////////////////////////////////////////////
					
					// Hide Task
					$.Deferred(function(task) {
						var oldPane = content.find(".pannel:visible");
						if (oldPane.length) {
							oldPane.stop(1, 0).animate({ opacity: 0}, 100, "linear", this.resolve);
						}
						else {
							this.resolve();
						}
					})
					
					// Resize Task
					.pipe(function() {
						allTabs.removeClass("active");
						return $.Deferred(function() {
							content.animate({ 
								height : newPannel.outerHeight(true)
							}, 100, "swing", this.resolve);
						});
					})
					
					// Show Task
					.pipe(function() {
						$tab.addClass("active").removeClass("loading");
						return $.Deferred(function() {
							newPannel.stop(1, 0).css({
								top : 0,
								position: "absolute"
							}).show().trigger("show").animate({ opacity: 1 }, 0, "linear", this.resolve);
						});
					})
					
					// Finalize Task
					.pipe(function() {
						var oldPane = content.find(".pannel:visible").not(newPannel);
						if (oldPane.length) {
							if (url && !$tab.closest(".tabs-widget").hasClass("keep-state")) {
								oldPane.hide().trigger("hide").trigger("beforeEmpty").empty();
							}
							else {
								oldPane.hide().trigger("hide");
							}
						}
						
						newPannel.css({
							overflow: "visible",
							height  : "auto",
							position: "relative"
						});
						
						content.css({
							overflow: "visible",
							height  : "auto",
							position: "relative"
						});
					});
				}
				
				if (!newPannel) {
					newPannel = url ? "" : content.find(".pannel").eq(allTabs.index($tab[0]));
					if (!newPannel.length) {
						newPannel = $('<div class="pannel"/>').css({
							position: "absolute",
							width   : "100%",
							top     : 0,
							opacity : 0
						}).appendTo(content);
					}
					$tab.data("panel", newPannel[0]);
					newPannel.on("ready.tabs", function(e) {
						if ( e.target === newPannel[0] ) {
							swap();
						}
						e.stopPropagation();
					});
				} else {
					newPannel = $(newPannel);
				}
				
				if (url && (newPannel.is(":empty") || !$tab.closest(".tabs-widget").hasClass("keep-state"))) {
					$.loadHTML(newPannel, $tab.attr("data-url"));
				}
				else {
					swap();
				}
			},
			"selectstart.tabs mousedown.tabs" : function(e) {
				e.preventDefault();
			}
		});
		
		$(".tabs-widget .pannel:not(:first)").hide();
		$(".tabs-widget .tab:first").trigger("click");
		
		$(window).on("newContent.initTabs", function(e) {
			$(".tabs-widget .pannel:not(:first)", e.target).hide();
			$(".tabs-widget .tab:first",  e.target).trigger("click");
		});
		
		$(window).bind("show.initTabs", function(e) {
			$(".tabs-widget .tab.active:first", e.target).trigger("click");
		});
	})();
	
	// Color pickers
	// =========================================================================
	(function() {
		
		var helper = $('<div/>').css({
			position : "absolute",
			width : 1,
			height: 1,
			margin: -1
		});
		
		helper.ColorPicker({
			onShow: function (colpkr) {
				$(colpkr).fadeIn(100);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(100);
				helper.removeData("targetInput");
				return false;
			},
			onSubmit: function (hsb, hex, rgb) {
				$(helper.data("targetInput")).val(hex).trigger("change");
			}
		}).appendTo("body");
		
		function doAttach( context ) {
			$('input[type="color"]', context).each(function() {
				var widget = $('<div class="color-input" />');
				var id     = this.id || "";
				var input  = $('<input type="text"/>').attr({
					value : String(this.value || "").replace(/^#*/, ""),
					name  : this.name  || "",
					size  : this.size  || 4
				}).appendTo(widget);
				
				var btn = $('<span class="color-button" />').appendTo(widget);
				
				btn.click(function() {
					if ( $(this).closest(".disabled").length ) {
						return false;
					}
					var offset = $(this).offset();
					helper.css({
						left : offset.left,
						top  : offset.top, 
						height: this.offsetHeight
					})
					.data("targetInput", input)
					.ColorPickerSetColor(input.val() || '#FFFFFF')
					.ColorPickerShow();
				});
				
				input.bind("change.cp", function() {
					btn.css('backgroundColor', this.value ? '#' + this.value : "transparent");
				}).triggerHandler("change.cp");
				
				$(this).replaceWith(widget);
				
				if (id) {
					input.attr("id", id);
				}
			});
		}
		
		$(window).on("newContent.attachColorPickers", function(e) {
			doAttach( e.target );
		});
		
		$(function() { doAttach( document ); });
		
		$(".colorpicker").live("mousedown.cp", false);
	})();
	
	// Sliders
	// =========================================================================
	(function() {
		
		function doAttach( context ) {
			$('input[type="range"]', context).each(function() {
				
				// The ID needs to be stored now and set to the new input AFTER 
				// this one is destroyed to prevent ID dublication
				var id = this.id || "";
				
				// This will replace the original range input
				var widget = $('<div class="range-widget" />');
				
				// Directly create RangeWidget instance as a singleton object
				var R = {
					widget    : widget,
					button    : $('<b class="range-button" />'          ).appendTo(widget),
					labelMin  : $('<div class="range-min"/>'            ).appendTo(widget),
					labelMax  : $('<div class="range-max"/>'            ).appendTo(widget),
					labelVal  : $('<div class="range-val"><span/></div>').appendTo(widget),
					path      : $('<div class="range-path" />'          ).appendTo(widget),
					input     : $('<input type="hidden" />'             ).appendTo(widget),
					min       : $.floatVal(this.getAttribute("min") || 0  ),
					max       : $.floatVal(this.getAttribute("max") || 100),
					lastValue : this.value || ""
				};
				
				R.range = R.max - R.min;
				R.step  = $.floatVal(this.getAttribute("step"), R.range / 100);
				
				// Cancel the native drag
				R.button.draggable = false;
				
				// Copy the "name" and "value" attributes from the original input
				R.input.attr({
					value : this.value || "",
					name  : this.name  || ""
				});
				
				R.widget.attr("tabindex", "-1");
				
				R.labelMin.text(this.getAttribute("data-minLabel") || R.min);
				R.labelMax.text(this.getAttribute("data-maxLabel") || R.max);
				
				R.setDataValue = function(x) {
					var maxWidth  = this.widget.width() - this.button.width();
					this.button[0].style.left = maxWidth * (x/this.range) + "px";
					$("span", this.labelVal).text(Math.round(x * 100) / 100);
				};
				
				$(this).replaceWith(widget);
				
				// If ID set it after the replacement
				if (id) {
					R.input.attr("id", id);
				}
				
				// Store the instance
				widget.data("RangeWidget", R);
				
				R.setDataValue(R.input.value || (R.max - R.min) / 2);
				
				id = widget = R = null;
			});
		}
		
		$("html").off(".RangeWidget")
		
		.on("newContent.RangeWidget", function(e) {
			doAttach( e.target );
		})
		
		.on ("change.RangeWidget", ".range-widget input", function(e, userDrag) { 
			
			// This is handled internaly
			if (userDrag) {
				return;
			}
			
			// This is to update the button position if some script sets the 
			// value and then triggers "change"
			var R = $(this).data("RangeWidget");
			if (R) {
				R.setDataValue(this.value);
			} 
		})
		
		.on ("mousedown.RangeWidget", ".range-widget", function(e) {
			
			// Always block selection 
			e.preventDefault();
			
			// Respect disabled state
			if ( $(this).closest(".disabled").length ) {
				return false;
			}
			
			var R = $(this).data("RangeWidget");
			if (!R) {
				return true;
			}
			
			R.widget.trigger("focus");
			
			var btnOffset = R.button.offset(),
				prtOffset = R.widget.offset(),
				prtWidth  = R.widget.width(),
				maxWidth  = prtWidth - R.button.width(),
				root      = $.browser.msie ? document : window,
				deltaX    = e.target === R.button[0] 
					? e.clientX - btnOffset.left 
					: R.button.width() / 2;
				
			// That is a "shared" div between multiple widgets so just show it 
			// in case it was found
			$("#overlay").show();
			
			R.button.addClass("active");
			
			$(root)
			.off(".dragSliderButton")
			.on({
				"mousemove.dragSliderButton" : function(e) {
					var l = Math.max(
						Math.min(e.clientX - deltaX - prtOffset.left, maxWidth),
						0
					);
					
					var part = maxWidth / (R.range/R.step);
					l = Math.round( l / part) * part;
					R.button[0].style.left = l + "px";
					
					var val = R.min + (l / maxWidth) * (R.range - R.min);
					val = Math.round(val * 100) / 100;
					$("span", R.labelVal).text(val);
					R.input.val(val).trigger("input", [true]);
				},
				"mouseup.dragSliderButton" : function() {
					$(this).off(".dragSliderButton");
					$("#overlay").hide();
					R.button.removeClass("active");
					if (R.input.val() !== R.lastValue) {
						R.lastValue = R.input.val();
						R.input.trigger("change", [true]);
					}
				}
			});
			
			// If the mousedown was not on the buttun move it to that position
			if (e.target !== R.button[0]) {
				$(root).trigger({
					type   : "mousemove.dragSliderButton",
					clientX: e.clientX
				});
			}
		});
		
		$(function() { doAttach( document ); });
	})();
	
	// Number inputs
	// =========================================================================
	(function() {
		function doAttach( context ) {
			$('input[type="number"]', context).each(function() {
				var wdgt = new $.NumberInput({
					value     : this.value,
					step      : $.floatVal(this.getAttribute("step") || 1, 1),
					min       : $.floatVal(this.getAttribute("min") || -Infinity),
					max       : $.floatVal(this.getAttribute("max") ||  Infinity),
					speed     : $.floatVal(this.getAttribute("data-speed") || 50, 50),
					delay     : $.floatVal(this.getAttribute("data-delay") || 300, 300)
				});
				
				wdgt.input.attr({
					name  : this.name  || "",
					size  : this.size  || ""
				});
				
				var id = this.id || "";
				
				$(this).replaceWith(wdgt.widget);
				
				if (id) {
					wdgt.input.attr("id", id);
				}
			});
		}
		
		$(function() { 
			$.loadScript("js/plugins/number_input/number_input.js", function() {
				doAttach( document );
				$(window).on("newContent.NumberInput", function(e) {
					doAttach( e.target );
				});
			});
		});
	})();
	
	// Toggle buttons
	// =========================================================================
	(function() {
		function doAttach( context ) {
			$('.toggle-button', context).each(function() {
				this.checked = $(this).hasClass("on");
				$(this).click(function() {
					$(this).toggleClass("on");
					this.checked = $(this).hasClass("on");
					$(this).trigger("change");
				});
			});
		}
		
		$(window).on("newContent.toggleButton", function(e) {
			doAttach( e.target );
		});
		
		$(function() { doAttach( document ); });
	})();
	
	// Radio group
	// =========================================================================
	(function() {
		function doAttach( context ) {
			$('.radio-group', context).each(function() {
				var widget = this;
				$("> *", this).click(function() {
					widget.value = this.getAttribute("data-value");
					$(this).addClass("on").siblings().removeClass("on");
					$(widget).trigger("change");
				})
				.filter(":first").addClass("first").end()
				.filter(":last" ).addClass("last" );
			});
		}
		
		$(window).on("newContent.radioGroup", function(e) {
			doAttach( e.target );
		});
		
		$(function() { doAttach( document ); });
	})();
	
	// Code Edit
	// Add (un)indent functionality to textareas using Tab or Shift + Tab
	// =========================================================================
    (function() {
        
		function getSelectionOffsets(textarea) {
			var result = {
				start : 0,
				end   : 0
			};
			
			if ("selectionStart" in textarea) {
				result.start = textarea.selectionStart;
				result.end = textarea.selectionEnd;
            }
			else if ( document.selection ) {
				
				var val    = textarea.value, 
					l      = val.length,
					range  = document.selection.createRange(),
					range2 = range.duplicate()
					sel    = range.text;
				
				range2.moveToElementText( textarea );
				
				range2.setEndPoint( 'StartToStart', range );
				
				var before = textarea.value.substring(0, l - range2.text.length);
				
				result.start = before.length;
				result.end   = before.length + sel.length;
				
				range = range2 = sel = val = before = l = null;
			}
			
			return result;
		}
		
		function indentSelection(textarea) {
			var selOffsets = getSelectionOffsets(textarea);
			var before     = textarea.value.substring(0, selOffsets.start);
			var after      = textarea.value.substring(selOffsets.end);
			var sel        = textarea.value.substring(selOffsets.start, selOffsets.end);
			var indent     = "    ";
			
			if (!sel.length) {
				sel = indent;
			}
			else {
				// If the selection does not start at the begining of line
				if ( before.length > 0 && !(/\r?\n$/).test(before)) 
				{
					// indexOf last EOL before the selection start
					var ni = before.search(/\r?\n[^(\r?\n)]*$/);
					
					// If the selection starts inside the first line
					if (ni === -1) { 
						sel = before + sel;
						before = "";
					}
					else {
						sel = before.substring(ni + 1) + sel;
						before = before.substring(0, ni + 1);
					}
				}
				sel = indent + sel.split(/\r?\n/).join("\r\n" + indent);
			}
			
			textarea.value = before + sel + after;
			setSelection(
				textarea,
				selOffsets.start + indent.length, 
				textarea.value.length - after.length
			);
			
			selOffsets = before = after = sel = indent = null;
		}
		
		function outdentSelection(textarea) {
			var selOffsets = getSelectionOffsets(textarea);
			var selStart = selOffsets.start;
			var selEnd   = selOffsets.end;
			var before   = textarea.value.substring(0, selStart);
			var after    = textarea.value.substring(selEnd);
			var sel      = textarea.value.substring(selStart, selEnd);
			var newStart = selStart;
			var indent   = "    ";
			var i;
			
			if (!sel.length) {
				before = before.replace(/( {1,4}|\t)$/, "");
				textarea.value = before + sel + after;
				setSelection(
					textarea,
					before.length, 
					textarea.value.length - after.length
				);
			}
			else {
			
				// If the selection does not start at the begining of line
				if ( before.length > 0 && !(/[\r\n]/).test(before.charAt(before.length - 1))) 
				{
					// indexOf last EOL before the selection start
					var ni = before.search(/[\r\n][^\r\n]*$/);
					
					// If the selection starts inside the first line
					if (ni === -1) { 
						sel = before + sel;
						before = "";
					}
					else {
						sel = before.substring(ni + 1) + sel;
						before = before.substring(0, ni + 1);
					}
				}
				
				var rows = sel.split(/\r?\n/), row, sel2 = [];
				for (i = 0; i < rows.length; i++) 
				{
					row = rows[i];
					var r = (/^( {1,4}|\t)/), m = row.match(r);
					if (m && m[1]) {
						row = row.replace(r, "");
						if ( i === 0 ) {
							newStart -= m[1].length;
						}
					}
					sel2.push(row);
				}
				sel2 = sel2.join("\r\n");

				if (sel2 !== sel) {
					textarea.value = before + sel2 + after;
					setSelection(
						textarea,
						newStart, 
						textarea.value.length - after.length
					);
				}
			}
			
			selOffsets = selStart = selEnd = before = after = sel = indent = newStart = null;
		}
        
		function setSelection(textarea, start, end) {
			if (textarea.setSelectionRange) {
				textarea.setSelectionRange(
					start, 
					Math.max(end, start), start
				);
			}
			else if ( document.selection ) {
				textarea.focus();
				var tr = textarea.createTextRange(), i;

				//Fix IE from counting the newline characters as two seperate characters
				textarea.value.replace(/(\r\n)/g, function(all, eol, pos) {
					if (pos < start) {
						start -= 1;
					}
					if (pos < end) {
						end -= 1;
					}
				});
				
				tr.moveEnd('textedit',-1);
				tr.moveStart('character',start);
				tr.moveEnd('character',end - start);
				tr.select();
			}
		}
		
		$("body").off(".tabIndent").on("keydown.tabIndent", "textarea", function(e) {
            if (e.keyCode == 9) { // Tab
                if (e.shiftKey) {
					outdentSelection(this);
				}
				else {
					indentSelection(this);
				}
				return false;
            }
        });
    })();
	
	// Block Selection
	// =========================================================================
	$("body").on("selectstart.blockSelection", function(e) {
		if ($(e.target).is(":disabled, .disabled, .disabled *, :disabled *")) {
			return false;
		}
		return $(e.target).is("input, select, textarea, [contenteditable], .selectable, .code,  .code *, .selectable *");
	});
	
	$.extendByTemplate = function(dataFrom, dataTo, propsList) {
		var _changed = false;
		$.each(propsList, function(i, name) {
			if ( name in dataFrom ) {
				if (dataTo[name] !== dataFrom[name]) {
					dataTo[name] = dataFrom[name];
					_changed = true;
				}
			}
			else {
				if ( name in dataTo ) { 
					delete dataTo[name];
					_changed = true;
				}
			}
		});
		return _changed;
	};
	
	$.makeMap = function(input, fillValue){
		var obj   = {}, 
			items = $.isArray(input) ? input : input.split(/\s*,\s*/),
			val   = arguments.length < 2 ? true : fillValue;
		for ( var i = 0; i < items.length; i++ ) {
			obj[ items[i] ] = val;
		}
		return obj;
	};
	
	
	
	
})(jQuery);

