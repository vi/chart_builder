/**
 * Editor ( base ) -------------------------------------------------------------
 */
function Editor() {
	this.widget = null;
	this.label = null;
}

Editor.prototype.init = function() {
	this.widget = $('<input type="text" />');
};


/** ----------------------------------------------------------------------------
 * Opacity
 */
OpacityEditor.prototype = new Editor();
function OpacityEditor() {
	
}

OpacityEditor.prototype.init = function() {
	this.widget = $('<input type="text" data-type="range" min="0" max="1" step="0.01" />');
};

/** ----------------------------------------------------------------------------
 * Color
 */
ColorEditor.prototype = new Editor();
function ColorEditor() {

}

ColorEditor.prototype.init = function() {
	this.widget = $('<input type="text" data-type="color" />');
};








////////////////////////////////////////////////////////////////////////////////

"Title" : {
	"Visible"    : CheckboxEditor(),
	"Text"       : TextEditor(),
	"Dimensions" : DimensionsEditor()
	"Position"   : DimensionsEditor()
	"Margins"    : DimensionsEditor()
	"Dimensions" : DimensionsEditor()
	"Dimensions" : DimensionsEditor()
}

////////////////////////////////////////////////////////////////////////////////

Font
	Face
	Bold
	Italic
	Underline
	Size
	Color
	Align
	Text
	
Background
	Solid
		Color
		Opacity
	Image
		File
		Position
		Repeat
		Transparent
		Fit
	
Border
Shadow
Color
Bevel
Opacity
TextAlign
Position
Dimensions
Margins