/*global window, jQuery, zingchart, Class, SKIP_THIS_PROPERTY*/

// The namespace
window.ChartBuilder = (function($, NS) {
	
	"use strict";
	
	var CONFIG = {
	    DEBUG    : true,
		CHART_ID : "chart", // DOM id
		sidebarWidth : 400, // todo
		bottomPanelHeight : 200, // todo
		supportedEngines : {
			// Name      : path
			//"Flot"
			//"Highcharts"
			"Zingcharts" : {
			    path      : "core/zingcharts/engine.js",
			    className : "ZingchartsEngine"
			}
		}
	};
	
	// Make the namespace (ChartBuilder) Observable and Configurable
	$.extend(NS, new NS.Observable(), new NS.Configurable(CONFIG));
	
	NS.staus = new NS.StatusBar("#footer");
	NS.staus.set("message", "Loading...");
	
	/**
     * Returns the list of supported chart engines
     */
    NS.getSupportedChartEngines = function() {
        return CONFIG.supportedEngines;
    };
    
    /**
     * Chart engine factory
     */
    (function() {
        
        var _loadedEngines = {}, 
            _currentEngine;
        
        NS.getChartEngine = function(name) {
            if(!name) {
                return _currentEngine;
            }
            
            if(!_loadedEngines.hasOwnProperty(name)) {
                if(!CONFIG.supportedEngines.hasOwnProperty(name)) {
                    throw "Unknown charting engine '" + name + "'";
                }
            }
            
            return _loadedEngines[name];
        };
        
        NS.setChartEngine = function(name) {
            var _oldName;
            if(_currentEngine) {
                _oldName = _currentEngine.name;

                // Nothing to change
                if(_oldName == name) {
                    return _currentEngine;
                }
            }

            var e = new NS.Event("beforeChartEngineChange", {
                oldEngine : _currentEngine
            });
            
            this.dispatchEvent(e);

            if(_currentEngine) {
                _currentEngine.uninit();
            }

            if(!e.isStopped()) {

                // Lazy load chart engine
                var self = this;
                NS.staus.set("message", 'Loading chart engine "' + name + '"');
                $.loadScript(
                    CONFIG.supportedEngines[name].path,
                    function() {
                        _loadedEngines[name] = new NS[
                            CONFIG.supportedEngines[name].className
                        ](CONFIG.CHART_ID);
                        _loadedEngines[name].name = name;
                        var oldEngine = _currentEngine;
                        _currentEngine = _loadedEngines[name];
                        self.dispatchEvent("afterChartEngineChange", {
                          oldEngine : oldEngine,
                          newEngine : _currentEngine
                        });
                    }
                );
            }
        };
        
    })();
    
    // Attach some event listeners ---------------------------------------------
    $(window).bind("error", function(e) {
        NS.Logger.error(e.originalEvent.message || "Error");
    });
    
    $("body").ajaxError(function(e, jqxhr, settings, exception) {
        NS.Logger.error(
            "Error requesting page %q <b>%s</b>", 
            settings.url, 
            exception
        );
    });
    
    // Add the debugging bottom panels if needed
    // ---------------------------------------------------------------------
    if (CONFIG.DEBUG) {
        $('<a class="tab" data-url="core/panels/log.html">Log</a>')
        .appendTo("#bottom-pane .tabs-widget .tabs");
        $('<a class="tab" data-url="core/panels/stats.html">Statistics</a>')
        .appendTo("#bottom-pane .tabs-widget .tabs");
    }

	// Class ChartEngines - the base class for charting engines
	// =========================================================================
	NS.ChartEngine = NS.Observable.extend({
        
	    uninit : function() {
			if(this.chart) {
				NS.dispatchEvent("beforeChartEngineUnload", {
					engine : this
				});
				this._uninit();
				this.chart = null;
				NS.dispatchEvent("afterChartEngineUnload", {
					engine : this
				});
			}
		}
	});

	
	
	$(function() {

		// Chart Engine selector -----------------------------------------------
		(function() {
			var engines = NS.getSupportedChartEngines();

			$.each(engines, function(name) {
				$("#engine").append('<option value="' + name + '">' + name + '</option>');
			});

			$("#engine").change(function() {
				NS.setChartEngine($(this).val());
			});

			NS.addEventListener("afterChartEngineChange", function(e) {
				$("#engine").val(e.data.newEngine.name);
			});
			engines = null;
		})();

		// Cleanup the side bar ("afterChartEngineUnload -----------------------
		NS.addEventListener("afterChartEngineUnload", function(e) {
			$("#editors").html('<dl/>');
		});
		
		// Vertical Resizer
		$("#separator-y").bind({
			mousedown : function(e) {
				var sep = $(this).addClass("active");
				$("#overlay").show();
				var delta = e.clientX - sep.offset().left;
				var max = $(window).width() - 100, min = 100;
				$(window).bind({
					"mousemove.dragResizer" : function(e) {
						sep.css("left", Math.max(Math.min(e.clientX - delta, max), min));
						return false;
					},
					"mouseup.dragResizer" : function(e) {
						$(window).unbind(".dragResizer");
						$("#overlay").hide();
						sep.removeClass("active");
						var left = sep.offset().left;
						$("#sidebar").width(left - 1);
						$("#main").css("left", left + 6);
						$(window).trigger("resize");
					}
				});
				return false;
			}
		});

		// Horizontal Resizer
		$("#separator-x").bind({
			mousedown : function(e) {
				var sep = $(this).addClass("active");
				$("#overlay").show();
				var delta = e.clientY - sep.position().top;
				var max = $(window).height() - 200, min = 100;
				$(window).bind({
					"mousemove.dragResizer" : function(e) {
						sep.css("top", Math.max(Math.min(e.clientY - delta, max), min));
						return false;
					},
					"mouseup.dragResizer" : function(e) {
						$(window).unbind(".dragResizer");
						$("#overlay").hide();
						sep.removeClass("active");
						var top = sep.position().top;
						//$("#preview").height(top);
						$("#bottom-pane").height($("#main").height() - (top + 7));
						$("#preview").css("bottom", $("#bottom-pane").height() + 7);
						sep.css({
							top : "auto",
							bottom : $("#bottom-pane").height() + 1
						});
						$(window).trigger("resize");
					}
				});
				return false;
			}
		});

		// Select the initial Chart engine -------------------------------------
		NS.setChartEngine("Zingcharts");

	});
	return NS;

})(jQuery, window.ChartBuilder || {});
