/*global window, jQuery, zingchart, Class*/

window.ChartBuilder = (function(NS, $) {
	
	"use strict";
	
	var _classes = [];
	var _instances = {};
	
	NS.getDeclaredClasses = function() {
		return _classes;
	};
	
	NS.getInstances = function() {
		return _instances;
	};
	
	/* Simple JavaScript Inheritance
	 * By John Resig http://ejohn.org/
	 * MIT Licensed.
	 */
	// Inspired by base2 and Prototype
	// Some modifications by vlad too...
	(function() {
		var initializing = false, 
			fnTest = /var a/.test(function(){ var a;}) ? /\b_super\b/ : /.*/;

		// The base Class implementation (does nothing)
		window.Class = function() {};

		// Create a new Class that inherits from this class
		Class.extend = function extend(prop, className) {
			
			_classes.push(className || "Class");
			
			var _super = this.prototype;

			// Instantiate a base class (but only create the instance,
			// don't run the init constructor)
			initializing = true;
			var prototype = new this();
			initializing = false;

			// Copy the properties over onto the new prototype
			for (var name in prop) {
				// Check if we're overwriting an existing function
				prototype[name] = typeof prop[name] == "function" &&
				typeof _super[name] == "function" && fnTest.test(prop[name]) ? 
				(function(name, fn) {
					return function() {
						var tmp = this._super;

						// Add a new ._super() method that is the same method
						// but on the super-class
						this._super = _super[name];

						// The method only need to be bound temporarily, so we
						// remove it when we're done executing
						var ret = fn.apply(this, arguments);       
						this._super = tmp;

						return ret;
					};
				})(name, prop[name]) : prop[name];
			}
			
			prototype.__destruct = function() {
				if ($.isFunction(this.uninit)) {
					this.uninit();
				}
				var x;
				for ( x in this) {
					if ( this.hasOwnProperty(x) ) {
						delete this[x];
					}
				}
				x = null;
				if (_instances[className || "Class"] !== undefined ) {
					_instances[className || "Class"]--;
				}
			};

			// The dummy class constructor
			function Class() {
				
				if (_instances[className || "Class"] !== undefined ) {
					_instances[className || "Class"]++;
				}
				else {
					_instances[className || "Class"] = 1;
				}
				
				// All construction is actually done in the init method
				if ( !initializing && this.init ) {
					this.init.apply(this, arguments);
				}
			}

			// Populate our constructed prototype object
			Class.prototype = prototype;

			// Enforce the constructor to be what we expect
			Class.prototype.constructor = Class;

			// And make this class extendable
			Class.extend = extend;

			return Class;
		};
	})();
	
	
	/**
     * Class Event
     */
	var Event = Class.extend({
		
		/**
		 * Class Event
		 * @param {String} The type of the event
		 * @param {Object} The data to store
		 * @constructor
		 */
		init : function Event(type, data) {
			
			/**
			 * Flag to indicate if the event was stopped
			 * @type {Boolean}
			 */
			var _stopped = false;

			/**
			 * Flag to indicate if the default handler of the event was prevented
			 * @type {Boolean}
			 */
			var _defaultPrevented = false;
			
			var tmp = type.split(".");
			
			/**
			 * The type of the event
			 * @type {String}
			 */
			this.type = $.trim(tmp[0]).toLowerCase();
			
			/**
			 * The namespace of the event
			 * @type {String}
			 */
			this.namespace = $.trim(tmp[1] || "");
			
			tmp = null;

			/**
			 * The data property
			 * @type {Object}
			 */
			this.data = data && typeof data == "object" ? data : {};

			/**
			 * Mark the event as stopped
			 */
			this.stop = function() {
				_stopped = true;
			};

			/**
			 * Mark the event as default-prevented
			 */
			this.preventDefault = function() {
				_defaultPrevented = true;
			};

			/**
			 * Test if the event has been stopped 
			 * @return {Boolean}
			 */
			this.isStopped = function() {
				return _stopped;
			};

			/**
			 * Test if the event has been default-prevented
			 * @return {Boolean} 
			 */
			this.isDefaultPrevented = function() {
				return _defaultPrevented;
			};
		}
	}, "Event");
	
	/**
	 * Class Observable. This is the base class for most of the objects
	 */
	var Observable = Class.extend({
		
		/**
		 * All the methods are defined within the constructor because they have 
		 * to deal with the private _eventListeners collection.
		 * @constructor
		 */ 
		init : function() {
			
			/**
			 * Contains all the event listeners grouped by event name
			 * @type {Object}
			 * @private
			 */
			var _eventListeners = {};
            
			/* 
			 * The following is very useful for debugging and global cleanup.
			 */
			(function(self) {
				
				/**
				 * This should report the current listeners by augmenting the 
				 * proxy array supplied by the COLLECT_EVENT_LISTENERS event.
				 */
				function reportHandlers(e, collection) {
					collection.push({
						target    : self,
						listeners : _eventListeners
					});
				}
				
				/* The following is very useful for debugging and global cleanup. 
				 * Any global code will be able to gain access to all the 
				 * (otherwise private) event handlers and their target objects 
				 * by firing the special "COLLECT_EVENT_LISTENERS" event 
				 * this way:
				 * 
				 * var handlers = [];
				 * $(window).trigger("COLLECT_EVENT_LISTENERS", [handlers]);
				 * // "handlers" now is filled with all the current listeners...
				 */
				$(window).bind("COLLECT_EVENT_LISTENERS", reportHandlers);
			
				/**
				 * Auto clean-up events on unload
				 */
				$(window).one("unload", function() {
					$(window).unbind("COLLECT_EVENT_LISTENERS", reportHandlers);
					_eventListeners = null;
				});
				
			})(this);
			
            /**
             * Adds a handler to the specified event. The implementation also 
			 * supports namespaced events. Examples:
			 *     "click"        -> Event type = "click", namespace = ""
			 *     "click.ns"     -> Event type = "click", namespace = "ns"
			 *     "click.ns a.b" -> Event type = "click", namespace = "ns a"
			 * The event type part of the first argument is case insensitive.
			 *
             * @param {String} eventType  The event type (including namespace).
             * @param {function} callback The event handler
             * @param {boolean} once      Auto-removed listener if TRUE
             */
            this.addEventListener = function(eventType, callback, once) {
                var m    = eventType.split("."),
					type = m[0].toLowerCase(),
					ns   = m[1] || "";
				
                
                // The first time we need to create a listeners collection
                if (!_eventListeners[type]) {
                    _eventListeners[type] = [];
                }
                
                var index = _eventListeners[type].length;
				
                if (once) { // self removing listener
                    _eventListeners[type][index] = {
						namespace : ns,
						handler : function() {
							callback.apply(this, arguments);
							_eventListeners[type].splice(index, 1);
						}
					};
                }
                else {
                    _eventListeners[type][index] = {
						namespace : ns,
						handler   : callback
					};
                }
            };
            
            /**
             * Removes event listener(s). Examples:
			 * 
			 * removeEventListener("*");       -> Remove EVERYTHING!
			 * removeEventListener("click");   -> Remove all click handlers
			 * removeEventListener("click.ns");-> Remove all click handlers having the "ns" namespace
			 * removeEventListener(".ns");     -> Remove all handlers having the "ns" namespace
			 * removeEventListener(".*");      -> Remove all namespaced handlers
			 * 
			 * All of the above examples are additionaly filtered by comparing 
			 * the the handler with the callback function if one is specified.
			 * The event type part of the first argument is case insensitive.
			 *
             * @param {String} eventType The event type (including namespace).
             * @param {Function} callback The event handler
             */
            this.removeEventListener = function(eventType, callback) {
                var m    = eventType.split("."),
					ns   =  m[1] || "",
					type = (m[0] || ns ? "*" : "").toLowerCase(),
					handlers, eType, i, l;
				
				for ( eType in _eventListeners ) {
					if ( type == "*" || type == eType ) {
						handlers = _eventListeners[eType]; 
						l = handlers.length;
						for ( i = 0; i < l; i++ ) {
							if ( ns == "*" ||                // ".*"
								(type == "*" && !ns) ||      // "*"
								(handlers[i] && ns == handlers[i].namespace)  // ".namespace"
							) {
								if ( !callback || handlers[i].handler === callback ) {
									handlers.splice(i, 1);
								}
							}
						}
					}
				}
				
				//m = ns = type = handlers = eType = i = l = null;
            };
            
            /**
             * Dispaches the event using this object as event's target
             * @param {Event} Event
             * @param {boolean} deferred
             */
            this.dispatchEvent = function(event) {
                var ns = "", custom = false;
				if (typeof event == "string") {
                    event = new Event( event, arguments[1] );
					ns = event.namespace;
					custom = true;
                }
                
				var type = event.type.toLowerCase();
				var args = Array.prototype.slice.call(arguments, 1);
				args.unshift(event);
				var returnValue = true, result;
				if (_eventListeners.hasOwnProperty(type)) {
                    for ( var i = 0, l = _eventListeners[type].length, cur; i < l; i++ ) {
                        cur = _eventListeners[type][i];
						if ( !ns || cur.namespace == ns ) {
							result = cur.handler.apply(this, args);
							if (result === false || event.isStopped()) {
								returnValue = false;
								break;
							}
						}
                    }
					i = l = cur = null;
                }
                
                if (custom) {
					//setTimeout(function() {
						event.__destruct();
						//event = null;
					//}, 0);
                }
                
				ns = custom = type = args = result = null;
                return returnValue;
            };
            
            /**
             * Provide access to the _eventListeners collection.
             * @return {Object}
             */
            this.listenersList = function() {
				return _eventListeners;
            };
			
			/**
			 * The destructor shpuld purge any event listeners
			 */
			this.unInit = function() {
				_eventListeners = null;
			};
		}
	}, "Observable");
	
	/** 
     * Class Configurable
     */
	var Configurable = Class.extend({
		init : function Configurable(options) {
			var _cfg = {};
			
			this.config = $.extend(new Observable(), {
				set : function(name, value) {
					_cfg[name] = value;
					this.dispatchEvent("change");
				},
				
				get : function(name) {
					return name ? _cfg[name] : _cfg; // Can be undefined!
				},
				
				has : function(name) {
					return name in _cfg;
				}, 
				
				extend : function(cfg, deep) {
					if (deep !== false) {
						$.extend(true, _cfg, cfg || {});
					}
					else {
						$.extend(_cfg, cfg || {});
					}
					this.dispatchEvent("change");
				}
			});
			
			if (options) {
				this.config.extend(options);
			}
		}
	}, "Configurable");
	
	/**
	 * The Logger class
	 */
	var Logger = Observable.extend({
		
		init : function() {
			
		    // First init the Observable
            this._super();
            
			var _log = [];
			var _logLimit = 100;
			var _logLength = 0;
			var _logMsgCounter = 0;
			var _now = $.now();
						
			function doLog(self, msg, type) {
				var m = { 
					type    : type, 
					message : msg, 
					time    : $.now() - _now,
					id      : ++_logMsgCounter
				};
				
				if ( _logLength >= _logLimit ) {
					var oldItemsLength = _logLength - _logLimit + 1;
					var oldItems = [];
					for (var i = 0; i < oldItemsLength; i++) {
						oldItems[i] = _log[i].id;
					}
					_log.splice(0, oldItemsLength);
					self.dispatchEvent("spliceLog", { itemsIDs: oldItems });
					oldItems = oldItemsLength = null;
				}
				_logLength = _log.push(m);
				
				self.dispatchEvent("log", m);
			}
			
			/**
			 * Returns the current log limit
			 * @return {int}
			 */
			this.getLogLimit = function() {
				return _logLimit;
			};
			
			/**
			 * Sets the log limit.
			 * @return {int}
			 */
			this.setLogLimit = function(n) {
				n = $.intVal(n);
				if (n > 0) {
					_logLimit = n;
				}
			};
			
			/**
			 * Logs a message
			 */
			this.log = function(msg) {
				doLog(this, $.sprintf.apply(null, arguments), "message");
			};
			
			/**
			 * Logs a warning
			 */
			this.warn = function(msg) {
				doLog(this, $.sprintf.apply(null, arguments), "warning");
			};
			
			/**
			 * Logs an warning
			 */
			this.error = function(msg) {
				doLog(this, $.sprintf.apply(null, arguments), "error");
			};
			
			/**
			 * Clears the log array (optionaly for the given category only)
			 */
			this.clearLog = function(type) {
				if (!type) {
					_log.splice(0, _log.length);
				}
				else {
					var l = _log.length;
					while (l) {
						if (_log[--l].type == type) {
							_log.splice(l, 1);
						}
					}
				}
				this.dispatchEvent("clearLog");
			};
			
			/**
			 * Returns the log array
			 */
			this.getLog = function() {
				return _log;
			};
		}
	}, "Logger");
	
	/**
	 * Class StatusBar
	 */
	var StatusBar = Class.extend({
	    
	    init : function( container ) {
	        this.container = $(container).html(
	            '<div class="statusbar-slot first last" id="statusbar-message"><span/></div>'
	        );
	    },
	    
	    set : function(slotID, text) {
	        $("#statusbar-" + slotID + " span", this.container).text(text);
	    },
	    
	    get : function(slotID) {
	        return $("#statusbar-" + slotID + " span", this.container).text();
	    },
	    
	    appendSlot : function(slotID, label, initialValue) {
	        var slot = $("#statusbar-" + slotID, this.container);
	        if (!slot.length) {
	            slot = slot = $(
                   '<div class="statusbar-slot" id="statusbar-' + slotID + '">' +
                   '<label></label><span/>' +
                   '</div>'
                )
                .appendTo(this.container);
	        }
	        $("label", slot).text(label + ": ")
            $("span" , slot).text(initialValue || initialValue === 0 ? initialValue : "");
	        $(".statusbar-slot.last", this.container).removeClass("last");
	        slot.addClass("last");
	    },
	    
	    prependSlot : function(slotID, label, initialValue) {
	        var slot = $("#statusbar-" + slotID, this.container);
            if (!slot.length) {
                slot = $(
                   '<div class="statusbar-slot" id="statusbar-' + slotID + '">' +
                   '<label></label><span/>' +
                   '</div>'
                )
                .prependTo(this.container);
            }
            $("label", slot).text(label + ": ")
            $("span" , slot).text(initialValue || initialValue === 0 ? initialValue : "");
            $(".statusbar-slot.first", this.container).removeClass("first");
            slot.addClass("first");
	    },
	    
	    removeSlot : function(slotID) {
	        if (slotID != "statusbar-message") {
	            $("#statusbar-" + slotID, this.container).remove();
	        }
	        $(".statusbar-slot").removeClass("first last")
	        .filter(":first").addClass("first").end()
	        .filter(":last").addClass("last");
	    },
	    
	    purge : function() {
	        $(".statusbar-slot:not(#statusbar-message", this.container).remove();
	        $("#statusbar-message").addClass("first last");
	    }
	}, "StatusBar");
	
	NS.Event        = Event;
	NS.Observable   = Observable;
	NS.Configurable = Configurable;
	NS.Logger       = new Logger();
	NS.StatusBar    = StatusBar;
	
	return NS;
	
})(window.ChartBuilder || {}, jQuery);
	