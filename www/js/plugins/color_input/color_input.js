
(function() {
	
	/**
	 * Class ColorInput
	 */
	function ColorInput(options)
	{
		if ( !( this instanceof ColorInput) ) {
			return new ColorInput(options);
		}
		
		this.widget = $(
			'<div class="color-input-wrapper">'
		+		'<input type="text" />'
		+		'<div class="color-picker-trigger"/>'
		+	'</div>'
		);
		
		this.input  = this.widget.find("input");
		this.button = this.widget.find(".color-picker-trigger");
		
		// Attach event listeners
		// =====================================================================
		var self = this;
	}

})();