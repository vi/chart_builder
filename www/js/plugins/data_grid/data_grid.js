(function($) {
    
    var _defaultData = {
        cols : [
            {
                label : ""
            }
        ],
        rows : []
    };
    
    function DataGrid( container ) {
        
        this.container = $(container).empty();
        
        this.widget = $(
             '<div class="data-grid">'
            +    '<div class="data-grid-scroll-x">'
            +        '<div class="data-grid-header"/>'
            +        '<div class="data-grid-body"/>'
            +    '</div>'
            +'</div>'
        ).appendTo(this.container);
    }
    
    DataGrid.prototype.build = function(data) {
        
        var colCount = data.cols.length,
            colIndex = -1,
            rowCownt = data.rows.length,
            colIndex = -1;
        
        var header = $('<table><tr><th/></tr></table>');
        var body   = $('<table><tr><td colspan="' + colCount + '"/></tr></table>');
        
        // header
        $.each(data.cols, function(i, col) {
            header.append(
                $('<th/>').text(col.label)
            );
        });
        $("th:first", header).remove();
        
        
        // body
        $.each(this.data.rows, function(rowIndex, cells) {
            var row = $('<tr/>'), l = Math.min(cells.length, colCount);
            for (var i = 0; i < l; i++) {
                var cell = $('<td/>').html(cells[i]);
                cell.appendTo(row);
            }
            row.appendTo(body);
        });
        $("tr:first", body).remove();
        
        $(".data-grid-header", this.container).empty().append(header);
        $(".data-grid-body", this.container).empty().append(body);
        
    };
    
    $.fn.dataGrid = function() {
        return this.each(function() {
            var instance = $.data(this, "DataGrid");
            if(!instance) {
                instance = new DataGrid(this);
                $.data(this, "DataGrid", instance);
            }
        });
    };
    
})(jQuery);