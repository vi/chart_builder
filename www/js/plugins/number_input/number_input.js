/*global window, jQuery, zingchart, Class*/
(function($) {
	
	/**
	 * This widget is used hundrets of times, so for performace reasons it uses 
	 * event delegation to handle it's events
	 */
	function setDelegates() {
		
		var NS = "NumberInput", 
			mw = ($.browser.mozilla ? "DOMMouseScroll." : "mousewheel.") + NS;
		
		$("body")
		
		.off("." + NS, ".num-editor, .num-editor *")
		
		.on("mousedown." + NS, ".num-editor-btn", function stepNumInput(e) {
			if (e.which === 1) {
				if (!$(this).closest(".disabled").length) {
					var inst = $(this).data(NS);
					if (inst) {
						inst.input.trigger("focus");
						inst[$(this).is(".up") ? "goUp" : "goDown"]();
						$(this).addClass("active");
					}
				}
				return false;
			}
		})
		
		.on("dblclick." + NS + " selectstart." + NS + " focus." + NS, ".num-editor", function preventSelection(e) {
			return e.target.nodeName == "INPUT" && !$(e.target).closest(".disabled").length;
		})
		
		.on("mouseup." + NS + " mouseleave." + NS, ".num-editor-btn", function stopTimer(e) {
			$(this).data(NS).stop();
			$(".num-editor-btn", this.parentNode).removeClass("active");
		})
		
		.on("focusout." + NS, ".num-editor > input", function stopTimer(e) {
			var inst = $(this).data(NS);
			if (inst) {
				$(window).unbind(mw);
				inst.widget.removeClass("focus");
				if (this.value !== inst._initialValue) {
					inst.input.trigger("change");
				}
			}
		})
		
		.on("change." + NS, ".num-editor > input", function(e, virtual) {
			var inst = $(this).data(NS);
			if (inst) {
				inst._initialValue = this.value;
			}
		})
		
		.on("focusin." + NS, ".num-editor > input", function stopTimer(e) {
			if ($(this).closest(".disabled").length) {
				return false;
			}
			
			var inst = $(this).data(NS);
			if (inst) {
				inst.widget.addClass("focus");
				inst._initialValue = this.value;
				$(window).unbind(mw).bind(mw, function(e) {
					var q = $.browser.mozilla 
					? e.originalEvent.detail     > 0 ? -1 : 1 
					: e.originalEvent.wheelDelta > 0 ? 1 : -1;
                    inst.sum(inst.options.step * q);
                    e.preventDefault();
                });
			}
		})
		
		.on("keydown." + NS, ".num-editor > input", function stopTimer(e) {
			var inst = $(this).data(NS);
			if (inst) {
				switch (e.keyCode) {
					case 38: // Up
						inst.sum(inst.options.step);
					return false;
					case 40: // Down
						inst.sum(-inst.options.step);
					return false;
				}
			}
		});
		
		setDelegates.done = true;
	}
	
	/**
	 * Class NumberInput
	 */
	function NumberInput( options )
	{
		if ( !( this instanceof NumberInput) ) {
			return new NumberInput(options);
		}
		
		if (!setDelegates.done) {
			setDelegates();
		}
		
		this._timer = null;
		
		this._delay = null;
		
		this._initialValue = "";
		
		this.options = $.extend({}, NumberInput.defaultSettings);
		
		this.widget = $(
			'<div class="num-editor">'
		+		'<input type="text" />'
		+		'<a class="num-editor-btn up"><b/></a>'
		+		'<a class="num-editor-btn down"><b/></a>'
		+	'</div>'
		);
		
		this.input   = this.widget.find("input").data("NumberInput", this);
		this.btnUp   = this.widget.find(".num-editor-btn.up").data("NumberInput", this);
		this.btnDown = this.widget.find(".num-editor-btn.down").data("NumberInput", this);
		
		this.configure( options );
	}
	
	/**
	 * The default settings
	 */
	NumberInput.defaultSettings = {
		//tabIndex  : "auto",
		value     : "",
		upTitle   : "Increase",
		downTitle : "Decrease",
		step      : 1,
		min       : -Infinity,
		max       : Infinity,
		speed     : 50,
		delay     : 300,
		onAttach  : $.noop,
		onInput   : $.noop,
		onChange  : $.noop,
		onDetach  : $.noop,
		prefix    : "",
		suffix    : ""
	};
	
	NumberInput.prototype = {
		
		configure : function( options )
		{
			// reset event handlers
			//this.options.onInput  = $.noop;
			//this.options.onChange = $.noop;
			//this.options.onAttach = $.noop;
			//this.options.onDetach = $.noop;
			
			$.extend(this.options, options);
			
			if (this.options.value || this.options.value === 0) {
    			var num = parseFloat(this.options.value);
    			if (num !== this.options.value) {
    				this.options.suffix = String(this.options.value).replace(num, "");
    				this.options.value = isNaN(num) ? "" : num;
    			}
    			
				if (this.options.value !== "") {
					this.options.value = Math.max(Math.min(num, this.options.max), this.options.min);
    			}
				
    			this.input.val(
    				this.options.prefix + 
    				this.options.value + 
    				this.options.suffix
    			);
			}
			
			if (!$.isFunction(this.options.onInput )) this.options.onInput  = $.noop;
			if (!$.isFunction(this.options.onChange)) this.options.onChange = $.noop;
			if (!$.isFunction(this.options.onAttach)) this.options.onAttach = $.noop;
			if (!$.isFunction(this.options.onDetach)) this.options.onDetach = $.noop;
			
			this.btnUp.attr("title", this.options.upTitle  );
			this.btnDown.attr("title", this.options.downTitle);
			//this.input.attr("tabIndex", this.options.tabIndex).val(this.options.value);
			
			this._initialValue = this.input.val();
			
			return this;
		},
		
		sum : function(step) 
		{
			var oldVlue  = $.floatVal(this.input.val()), 
				newValue = oldVlue + step;
		
			if (newValue % step > 0) {
				newValue = Math.floor(newValue / step) * step;
			}
			
			var _fixed = false;
			
			var min = $.floatVal(this.input.attr("min") || this.options.min);
			if ( newValue < min ) {
				newValue = min;
				_fixed = true;
			}
			
			var max = $.floatVal(this.input.attr("max") || this.options.max);
			if ( newValue > max ) {
				newValue = max;
				_fixed = true;
			}
			
			if ( newValue !== oldVlue ) {
				this.input.val(
					this.options.prefix + 
					newValue + 
					this.options.suffix
				).trigger("input");
				this.options.onInput.call(this.input[0], this.input[0], this);
			}
			
			return !_fixed;
		},
		
		step : function( addition ) 
		{
			if ( this.sum( addition ) ) {
				
				this._timer = setTimeout(
					
					(function(self) {
						return function() {
							self.step( addition );
						};
					})(this), 
					
					this.options.speed
				);
			}
		},
		
		goUp : function() 
		{
			this.stop();
			this.sum(this.options.step);
			this._delay = setTimeout(
				(function(self) {
					return function() {
						self.step( self.options.step );
					};
				})(this),
				this.options.delay
			);
		},
		
		goDown : function() 
		{
			this.stop();
			this.sum(-this.options.step);
			this._delay = setTimeout(
				(function(self) {
					return function() {
						self.step( -self.options.step );
					};
				})(this),
				this.options.delay
			);
		},
		
		stop : function() 
		{
			if (this._timer) clearTimeout(this._timer);
			if (this._delay) clearTimeout(this._delay);
		}
	};
	
	jQuery.NumberInput = NumberInput;
	
	// Load the widget's stylesheet and images
	var BASE = $('script[src$="number_input/number_input.js"]').attr("src")
		.replace(/number_input\.js$/, "");
	
	$.loadCSS( BASE + "style.css" );
	$.preloadImages(BASE + "arrows.gif");
	
})(jQuery);
