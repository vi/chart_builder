
(function($) {
  
  // Class Color 
  function Color() 
  {
    var _props = {
      hue   : 0,
      sat   : 0.5,
      lum   : 0.5,
      r     : 0,
      g     : 0,
      b     : 0,
      alpha : 1,
      name  : ""
    };
    
    function updateRGB() 
    {
      var rgb  = Color.hslToRgb(_props.hue, _props.sat, _props.lum);
      _props.r = rgb[0];
      _props.g = rgb[1];
      _props.b = rgb[2];
    }
    
    function updateHSL() 
    {
      var hsl    = Color.rgbToHsl(_props.r, _props.g, _props.b);
      _props.hue = hsl[0];
      _props.sat = hsl[1];
      _props.lum = hsl[2];
    }
    
    function change(name, value)
    {
      if ( name in _props ) {
        if ( _props[name] !== value ) {
          _props[name] = value;
          
          if (/^(r|g|b)$/.test(name)) 
          {
            updateHSL();
          }
          
          else if (/^(hue|sat|lum)$/.test(name)) 
          {
            updateRGB();
          }
        }
      }
    }
    
    // setters -----------------------------------------------------------------
    this.setAlpha = function(a) {
      change("alpha", Math.max(Math.min(a, 1), 0));
      return this;
    };
    
    this.setName = function(name) {
      change("name", String(name));
      return this;
    };
    
    this.setHue = function(hue) {
      change("hue", Math.max(Math.min(hue, 1), 0));
      return this;
    };
    
    this.setSaturation = function(sat) {
      change("sat", Math.max(Math.min(sat, 1), 0));
      return this;
    };
    
    this.setLuminance = function(lum) {
      change("lum", Math.max(Math.min(lum, 1), 0));
      return this;
    };
    
    this.setR = function(r) {
      change("r", Math.max(Math.min(r, 255), 0));
      return this;
    };
    
    this.setG = function(g) {
      change("g", Math.max(Math.min(g, 255), 0));
      return this;
    };
    
    this.setB = function(b) {
      change("b", Math.max(Math.min(b, 255), 0));
      return this;
    };
    
    
    // getters -----------------------------------------------------------------
    this.getAlpha = function() {
      return _props.alpha;
    };
    
    this.getName = function() {
      return _props.name;
    };
    
    this.getHue = function() {
      return _props.hue;
    };
    
    this.getSaturation = function() {
      return _props.sat;
    };
    
    this.getLuminance = function() {
      return _props.lum;
    };
    
    this.getR = function() {
      return _props.r;
    };
    
    this.getG = function() {
      return _props.g;
    };
    
    this.getB = function() {
      return _props.b;
    };
    
    this.getString = function(type) 
    {
      switch (type.toLowerCase()) {
        case "hex":
          return Color.rgbToHex(_props.r, _props.g, _props.b);
        case "rgb":
          return "rgb(" 
          + Math.round(_props.r) + ", "
          + Math.round(_props.g) + ", "
          + Math.round(_props.b) + ")";
        case "rgba":
          return "rgba(" 
          + Math.round(_props.r) + ", "
          + Math.round(_props.g) + ", "
          + Math.round(_props.b) + ", "
          + _props.alpha + ")";
        case "hsl":
          return "hsl(" 
          + Math.round(_props.hue * 3600)/10 + ", "
          + Math.round(_props.sat * 1000)/10 + "%, "
          + Math.round(_props.lum * 1000)/10 + "%)";
         case "hsla":
          return "hsla(" 
          + Math.round(_props.hue * 3600)/10 + ", "
          + Math.round(_props.sat * 1000)/10 + "%, "
          + Math.round(_props.lum * 1000)/10 + "%, "
          + _props.alpha + ")";
      }
    };
    
    // combined setters --------------------------------------------------------
    this.setRGB = function(r, g, b) 
    {
      _props.r = Math.max(Math.min(r, 255), 0);
      _props.g = Math.max(Math.min(g, 255), 0);
      _props.b = Math.max(Math.min(b, 255), 0);
      updateHSL();
      return this;
    };
    
    this.setHSL = function(h, s, l)
    {
      _props.hue = Math.max(Math.min(h, 1), 0);
      _props.sat = Math.max(Math.min(s, 1), 0);
      _props.lum = Math.max(Math.min(l, 1), 0);
      updateRGB();
      return this;
    };
    
    this.setString = function(str)
    {
      var c = Color.parse(str);
      this.setRGB(c.getR(), c.getG(), c.getB());
      c = null;
      return this;
    };
  }
  
  // Static --------------------------------------------------------------------
  Color.rgbToHex = function(r, g, b)
  {
    var hex = [
      Math.round(r).toString(16), 
      Math.round(g).toString(16), 
      Math.round(b).toString(16)
    ];
		$.each(hex, function (nr, val) {
			if (val.length == 1) {
				hex[nr] = '0' + val;
			}
		});
		return "#" + hex.join('');
  };
  
  /**
   * Converts an RGB color value to HSL. Conversion formula
   * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
   * Assumes r, g, and b are contained in the set [0, 255] and
   * returns h, s, and l in the set [0, 1].
   * 
   * Modified from http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-
   * color-model-conversion-algorithms-in-javascript
   *
   * @param   {Number}  r  The red color value
   * @param   {Number}  g  The green color value
   * @param   {Number}  b  The blue color value
   * @return  Object       The HSL representation
   */
  Color.rgbToHsl = function(r, g, b) 
  {
    r /= 255; g /= 255; b /= 255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;
    
    if(max == min){
      h = s = 0; // achromatic
    } else {
      var d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
      switch(max){
        case r: h = (g - b) / d + (g < b ? 6 : 0); break;
        case g: h = (b - r) / d + 2; break;
        case b: h = (r - g) / d + 4; break;
      }
      h /= 6;
    }
    
    return [ h, s, l ];
  };
  
  /**
   * Converts an HSL color value to RGB. Conversion formula
   * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
   * Assumes h, s, and l are contained in the set [0, 1] and
   * returns r, g, and b in the set [0, 255].
   * 
   * Modified from http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-
   * color-model-conversion-algorithms-in-javascript
   *
   * @param   {Number}  h       The hue
   * @param   {Number}  s       The saturation
   * @param   {Number}  l       The lightness
   * @return  Array           The RGB representation
   */
  Color.hslToRgb = function(h, s, l) 
  {
    var r, g, b;
    
    if (s === 0) {
      r = g = b = l; // achromatic
    } else {
      function hue2rgb(p, q, t){
        if (t < 0) 
        {
          t += 1;
        }
        if (t > 1) 
        {
          t -= 1;
        }
        if (t < 1 / 6) 
        {
          return p + (q - p) * 6 * t;
        }
        if (t < 1 / 2) 
        {
          return q;
        }
        if (t < 2 / 3) 
        {
          return p + (q - p) * (2 / 3 - t) * 6;
        }
        return p;
      }
      
      var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      var p = 2 * l - q;
      r = hue2rgb(p, q, h + 1/3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1/3);
    }
    
    return [ r * 255, g * 255, b * 255 ];
  };
  
  /**
   * Based on Color Conversion functions from highlightFade by Blair Mitchelmore
   * http://jquery.offput.ca/highlightFade/
   * Modified to support rgba and hsl/hsla colors
   * NOTE: the return value can be undefined!
   */
  Color.parse = function(color) 
  {
    var result;
    
    // Check if we're already dealing with an array of colors
    if (color && color.constructor == Array && (color.length == 3 || color.length == 4)) 
    {
      result = new Color().setRGB(color[0], color[1], color[2]);
      if (color.length == 4) {
        result.setAlpha(color[3]);
      }
      return result;
    }
    
    color = String(color);//.replace(/^\s*0x/, "#");
    
    // Look for #a0b1c2
    if ((result = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(color))) 
    {
      return new Color().setRGB(
        parseInt(result[1], 16), 
        parseInt(result[2], 16), 
        parseInt(result[3], 16)
      );
    }
    
    // Look for #fff
    if ((result = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(color))) 
    {
      return new Color().setRGB(
        parseInt(result[1] + result[1], 16), 
        parseInt(result[2] + result[2], 16), 
        parseInt(result[3] + result[3], 16)
      );
    }
    
    // Look for rgb(num,num,num)
    if ((result = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(color))) 
    {
      return new Color().setRGB(
        parseInt(result[1], 10), 
        parseInt(result[2], 10), 
        parseInt(result[3], 10)
      );
    }
    
    // Look for rgb(num%,num%,num%)
    if ((result = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(color))) 
    {
      return new Color().setRGB(
        parseFloat(result[1]) * 2.55, 
        parseFloat(result[2]) * 2.55, 
        parseFloat(result[3]) * 2.55
      );
    }
    
    // Look for rgba(num,num,num,num)
    if ((result = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*(0|1|0?\.[0-9]+)\s*\)/.exec(color))) 
    {
      return new Color().setRGB(
        parseInt(result[1], 10), 
        parseInt(result[2], 10), 
        parseInt(result[3], 10)
      ).setAlpha(parseFloat(result[4]));
    }
   
   // Look for rgba(num%,num%,num%,num)
    if ((result = /rgba\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*(0|1|0?\.[0-9]+)\s*\)/.exec(color))) 
    {
      return new Color().setRGB(
        parseFloat(result[1]) * 2.55, 
        parseFloat(result[2]) * 2.55, 
        parseFloat(result[3]) * 2.55
      ).setAlpha(parseFloat(result[4]));
    }
    
    // Look for hsl(num,num%,num%)
    if ((result = /hsl\(\s*([0-9]+(?:\.[0-9]+)?)\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(color))) 
    {
      return new Color().setHSL(
        parseFloat(result[1]) / 360,
        parseFloat(result[2]) / 100,
        parseFloat(result[3]) / 100
      );
    }
    
    // Look for hsla(num,num%,num%,num)
    if ((result = /hsla\(\s*([0-9]+(?:\.[0-9]+)?)\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*(0|1|0?\.[0-9]+)\s*\)/.exec(color))) 
    {
      return new Color().setHSL(
        parseFloat(result[1]) / 360,
        parseFloat(result[2]) / 100,
        parseFloat(result[3]) / 100
      ).setAlpha(parseFloat(result[4]));
    } 
    
    // Otherwise, we're most likely dealing with a named color
    color = $.trim(color).toLowerCase();
    if ((result = Color.namedColors[color])) {
      return new Color().setRGB(
        result[0], 
        result[1], 
        result[2]
      ).setName(color);
    }
    
    // in case of error
    return new Color().setRGB(0, 0, 0);
  };
  
  Color.fromRGB = function(r, g, b) 
  {
    return new Color().setRGB(r, g, b);
  };
  
  Color.fromHSL = function(h, s, l) 
  {
    return new Color().setHSL(h, s, l);
  };
    
  // Some named colors to work with
  Color.namedColors = {
    black:[0,0,0],
    darkgreen:[0,100,0],
    navy:[0,0,128],
    darkred:[139,0,0],
    midnightblue:[25,25,112],
    mediumblue:[0,0,205],
    forestgreen:[34,139,34],
    saddlebrown:[139,69,19],
    darkolivegreen:[85,107,47],
    firebrick:[178,34,34],
    brown:[165,42,42],
    red:[255,0,0],
    teal:[0,128,128],
    seagreen:[46,139,87],
    darkmagenta:[139,0,139],
    olivedrab:[107,142,35],
    sienna:[160,82,45],
    crimson:[220,20,60],
    limegreen:[50,205,50],
    dimgrey:[105,105,105],
    orangered:[255,69,0],
    darkgoldenrod:[184,134,11],
    chocolate:[210,105,30],
    mediumseagreen:[60,179,113],
    mediumvioletred:[199,21,133],
    darkviolet:[148,0,211],
    lawngreen:[124,252,0],
    steelblue:[70,130,180],
    springgreen:[0,255,127],
    slategrey:[112,128,144],
    indianred:[205,92,92],
    royalblue:[65,105,225],
    slateblue:[106,90,205],
    mediumspringgreen:[0,250,154],
    darkorchid:[153,50,204],
    lightslategrey:[119,136,153],
    yellowgreen:[154,205,50],
    cadetblue:[95,158,160],
    goldenrod:[218,165,32],
    orange:[255,165,0],
    deeppink:[255,20,147],
    tomato:[255,99,71],
    dodgerblue:[30,144,255],
    deepskyblue:[0,191,255],
    coral:[255,127,80],
    mediumslateblue:[123,104,238],
    gold:[255,215,0],
    rosybrown:[188,143,143],
    greenyellow:[173,255,47],
    mediumaquamarine:[102,205,170],
    palevioletred:[219,112,147],
    darkkhaki:[189,183,107],
    mediumorchid:[186,85,211],
    mediumturquoise:[72,209,204],
    cornflowerblue:[100,149,237],
    salmon:[250,128,114],
    turquoise:[64,224,208],
    sandybrown:[244,164,96],
    darksalmon:[233,150,122],
    darkgrey:[169,169,169],
    yellow:[255,255,0],
    lightgreen:[144,238,144],
    tan:[210,180,140],
    lightsalmon:[255,160,122],
    hotpink:[255,105,180],
    burlywood:[222,184,135],
    orchid:[218,112,214],
    palegreen:[152,251,152],
    skyblue:[135,206,235],
    lightskyblue:[135,206,250],
    lightsteelblue:[176,196,222],
    plum:[221,160,221],
    violet:[238,130,238],
    khaki:[240,230,140],
    lightblue:[173,216,230],
    thistle:[216,191,216],
    powderblue:[176,224,230],
    lightgrey:[211,211,211],
    palegoldenrod:[238,232,170],
    wheat:[245,222,179],
    pink:[255,192,203],
    paleturquoise:[175,238,238],
    peachpuff:[255,218,185],
    gainsboro:[220,220,220],
    moccasin:[255,228,181],
    bisque:[255,228,196],
    blanchedalmond:[255,235,205],
    antiquewhite:[250,235,215],
    papayawhip:[255,239,213],
    mistyrose:[255,228,225],
    lightgoldenrodyellow:[250,250,210],
    linen:[250,240,230],
    cornsilk:[255,248,220],
    oldlace:[253,245,230],
    lightyellow:[255,255,224],
    whitesmoke:[245,245,245],
    seashell:[255,245,238],
    lavenderblush:[255,240,245],
    aliceblue:[240,248,255],
    floralwhite:[255,250,240],
    mintcream:[245,255,250],
    ghostwhite:[248,248,255],
    snow:[255,250,250],
    white:[255,255,255]
  };
  
  // Expose Color 
  $.Color = Color;
  
  
})(jQuery);

////////////////////////////////////////////////////////////////////////////////
//                         Color Picker Plugin                                //
////////////////////////////////////////////////////////////////////////////////
(function($) {
	
	var BASE = $('script[src$="colorpicker/colorpicker.js"]')
		.attr("src").replace(/colorpicker\.js$/, "")
	
	// Load the widget's stylesheet
	$.loadCSS( BASE + "style.css" );
	$.preloadImages(BASE + "spectrum.png", BASE + "cross.gif", BASE + "lum.png");
	
	function ColorInput()
	{
		this.widget = $(
			'<div class="color-input-wrapper">'
		+		'<input type="text" />'
		+		'<div class="color-picker-trigger"/>'
		+	'</div>'
		);
	}
	
	
	function installTo(container) {
		$(container).html('<table cellpadding="0" cellspacing="0" border="0" unselectable="on">'
		+ '  <tr>'
		+ '    <td>'
		+ '      <div class="spectrum">'
		+ '        <img class="spectrum-gradient" src="' + BASE + 'spectrum.png" alt="Select Color" />'
		+ '        <img class="color-pos" src="' + BASE + 'cross.gif" />'
		+ '      </div>'
		+ '    </td>'
		+ '    <td width="10">&nbsp;&nbsp;</td>'
		+ '    <td>'
		+ '      <div class="lum">'
		+ '        <img class="lum-gradient" src="' + BASE + 'lum.png" />'
		+ '        <div class="lum-pos"></div>'
		+ '      </div>'
		+ '    </td>'
		+ '  </tr>'
		+ '</table>');
	}

	(function() {
		
		function showColorPickerPopUp(target) {
			var p = $("#color-picker-popup");
			if (!p.length) {
				p = $('<div id="color-picker-popup" class="color-picker" />').appendTo("body").dmxColorPicker();
			}
			
			var offsets = $(target).offset();
			p.css({
				top: offsets.top + $(target).outerHeight(),
				left : offsets.left
			});
			return p;
		}
		
		$.colorPickerPopUp = function(target, color)
		{
			var picker = showColorPickerPopUp(target);
			if (color) {
				setTimeout(function() {
					picker[0].setColor(color);
				}, 100);
			}
		}
		
	})();
  
  $.fn.dmxColorPicker = function(opt)
  {
    var cfg = $.extend({
      color: ""    // selected color
    }, opt);
    
    return this.each(function(i, o) 
    {
      installTo(o, cfg.base);
      var container    = $(o);
      var colorPointer = container.find(".color-pos");
      var spectrum     = container.find(".spectrum-gradient");
      var satGradient  = container.find(".sat-gradient");
      var lumGradient  = container.find(".lum-gradient");
      var lumPointer   = container.find(".lum-pos");
      var $lum         = container.find(".lum");
      var root         = $($.browser.msie ? document.documentElement : window);
      var minX         = 0;
      var minY         = 0;
      var maxX         = 0;
      var maxY         = 0;
      var deltaX       = 0;
      var deltaY       = 0;
      var offset       = { top: 0, left: 0 };
      var hue          = 0;
      var sat          = 0.5;
      var lum          = 0.5;
      var refreshed    = false;
      
      function moveColorPointer(x, y)
      {
        var _x = Math.min(Math.max(( x - deltaX ), minX), maxX);
        var _y = Math.min(Math.max(( y - deltaY ), minY), maxY);
        colorPointer[0].style.top  = _y + "px";
        colorPointer[0].style.left = _x + "px";
        hue = _x / maxX;
        sat = 1 - _y / maxY;
        var col = $.Color.fromHSL(hue, sat, 0.5);
        $lum.css("backgroundColor", col.getString("rgb"));
        col = null;
        calculateColor();
      }
      
      function moveLumPointer(y)
      {
        var _y = Math.min(Math.max(( y - deltaY ), minY), maxY);
        lumPointer[0].style.top  = _y + "px";
        lum = 1 - _y / maxY;
        calculateColor();
      }
      
      function calculateColor()
      {
        $(o).trigger("colorPreview", [$.Color.fromHSL(hue, sat, lum)]);
      }
      
      function refreshBox()
      {
        maxX   = spectrum.width();
        maxY   = spectrum.height();
        offset = spectrum.offset();
        deltaX = offset.left;
        deltaY = offset.top;
        refreshed = true;
      }
      
      colorPointer.bind("dblclick", function(e) { 
        $(container).trigger("colorSelect", [$.Color.fromHSL(hue, sat, lum)]);
      });
      
      lumPointer.bind("dblclick", function(e) { 
        $(container).trigger("colorSelect", [$.Color.fromHSL(hue, sat, lum)]);
      });  
      
      
      
      $(o).bind("mousedown.colorpicker", function(e) {
        if (!refreshed) {
          refreshBox();
        }
        
        if (e.target === spectrum[0] || 
            e.target === satGradient[0] || 
            e.target === colorPointer[0]) {
          moveColorPointer( e.pageX, e.pageY );
          root.bind("mousemove.colorpicker", function(e) {
            moveColorPointer( e.pageX, e.pageY );
          });
        }
        
        else if (e.target === lumGradient[0] || e.target === lumPointer[0]) {
          moveLumPointer(e.pageY);
          root.bind("mousemove.colorpicker", function(e) {
            moveLumPointer( e.pageY );
          });
        }
        
        root.one("mouseup.colorpicker", function(e) {
          $(this).unbind("mousemove.colorpicker");
        });
        
        return false;
      });
      
      $("table", o)
      .unbind("selectstart dragstart drag drop click")
      .bind("selectstart dragstart drag drop click", false);
      
      o.setColor = function(color) {
        var _c = color instanceof $.Color 
        ? color
        : typeof color == "string"
          ? $.Color.parse(color)
          : new $.Color();
        hue = _c.getHue();
        sat = _c.getSaturation();
        lum = _c.getLuminance();
        refreshBox();
        colorPointer.css({ left : maxX * hue, top  : maxY - (maxY * sat) });
        lumPointer.css({ top : maxY - (maxY * lum) });
        
        _c.setLuminance(0.5);
        $(".lum", container).css("backgroundColor", _c.getString("rgb"));
        
        _c.setLuminance(lum);
        $(container).trigger("colorPreview", [_c]);
        
        return this;
      };
      
      o.getColor = function(format) {
        var color = $.Color.fromHSL(hue, sat, lum);
        if (format) {
          color = color.getString(format);
        }
        return color;
      };
      
      if (cfg.color && spectrum.is(":visible")) {
        o.setColor(cfg.color);
      }
    });
   
  };
  
})(jQuery);