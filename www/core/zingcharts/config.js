var SVG    = 2;
var CANVAS = 4;
var FLASH  = 8;

ENV.applyConfig({
    
    editorsDir         : "core/zingcharts/editors",
    reusableEditorsDir : "core/zingcharts/editors/reusable",
    
    // =========================================================================
    // Chart Types
    // Listing all the available chart types and their display names
    // =========================================================================
    chartTypes    : {
		line      : "Line Chart",
		area3d    : "3D Area Chart",
		bar3d     : "3D Bar Chart",
		hbar3d    : "3D Horizontal Bar Chart",
		line3d    : "3D Line Chart",
		pie3d     : "3D Pie Chart",
		area      : "Area Chart",
		bar       : "Bar Chart",
		bubble    : "Bubble Chart",
		gauge     : "Gauge Chart",
		hbar      : "Horizontal Bar Chart",
		hbullet   : "Horizontal Bullet Chart",
		hfunnel   : "Horizontal Funnel Chart",
		nestedpie : "Nested Pie Chart",
		piano     : "Piano Chart",
		pie       : "Pie Chart",
		radar     : "Radar Chart",
		scatter   : "Scatter Chart",
		stock     : "Stock Chart",
		vbullet   : "Vertical Bullet Chart",
		vfunnel   : "Vertical Funnel Chart",
		hfunnel3d : "3D Horizontal Funnel Chart",
		vfunnel3d : "3D Vertical Funnel Chart",
		venn      : "Venn Chart"
	},
	
	// =========================================================================
    // Listing all the available editors
    // "label" will be the acordion panel label
    // =========================================================================
	editors : {
		general : {
			label : "General",
			url : "general.html"
		},
		title : {
			label : "Title",
			url : "title.html"
		},
		tooltips : {
			label : "Tooltips",
			url : "tooltips.html"
		},
		markers : {
			label : "Markers",
			url : "markers.html"
		},
		legend : {
			label : "Legend",
			url : "legend.html"
		},
		scales : {
			label : "Scales",
			url : "scales.html"
		},
		guides : {
			label : "Guides",
			url : "guides.html"
		},
		valueBox : {
			label : "Value Box",
			url : "valuebox.html"
		},
		preview : {
			label : "Preview",
			url : "preview.html"
		},
		zooming : {
			label : "Zooming",
			url : "zooming.html"
		},
		lens : {
			label : "Lens",
			url : "lens.html"
		},
		
		// Chart Specific ------------------------------------------------------
		area : {
		    label : "Chart Specific",
		    url   : "chart_specific/area.html"
		},
		area3d : {
		    label : "Chart Specific",
            url   : "chart_specific/area3d.html" 
		},
        bar : {
            label : "Chart Specific",
            url   : "chart_specific/bar.html" 
        },
        bar3d : {
            label : "Chart Specific",
            url   : "chart_specific/bar3d.html" 
        },
        bullet : {
            label : "Chart Specific",
            url   : "chart_specific/bullet.html"
        },
        
        funnel : {
            label : "Chart Specific",
            url   : "chart_specific/funnel.html"
        },
        funnel3d : {
            label : "Chart Specific",
            url   : "chart_specific/funnel3d.html"
        },
        line : {
            label : "Chart Specific",
            url   : "chart_specific/line.html"
        },
        line3d : {
            label : "Chart Specific",
            url   : "chart_specific/line3d.html"
        },
        nestedpie : {
            label : "Chart Specific",
            url   : "chart_specific/nestedpie.html"
        },
        piano : {
            label : "Chart Specific",
            url   : "chart_specific/piano.html"
        },
        pie : {
            label : "Chart Specific",
            url   : "chart_specific/pie.html"
        },
        pie3d : {
            label : "Chart Specific",
            url   : "chart_specific/pie3d.html"
        },
        radar : {
            label : "Chart Specific",
            url   : "chart_specific/radar.html"
        }
	},
	
	// =========================================================================
    // The chart types available for the given renderer
    // =========================================================================
    chartTypesForRenderer : {
	    svg    : "area,area3d,bar,bar3d,hbar,hbar3d,line,line3d,pie,pie3d,nestedpie,hfunnel          ,vfunnel          ,hbullet,vbullet,bubble,gauge,piano,radar,scatter,stock",
	    canvas : "area,area3d,bar,bar3d,hbar,hbar3d,line,line3d,pie,pie3d,nestedpie,hfunnel,         ,vfunnel,         ,hbullet,vbullet,bubble,gauge,piano,radar,scatter,stock",
	    flash  : "area,area3d,bar,bar3d,hbar,hbar3d,line,line3d,pie,pie3d,nestedpie,hfunnel,hfunnel3d,vfunnel,vfunnel3d,hbullet,vbullet,bubble,gauge,piano,radar,scatter,stock,venn"
	},
	
	// =========================================================================
    // The editors available for the given chart type
    // =========================================================================
	editorsForType : {
		area      : ["general", "area"     , "title", "tooltips", "legend", "valueBox", "markers", "scales", "guides", "preview", "zooming", "lens"],
		area3d    : ["general", "area3d"   , "title", "tooltips", "legend", "valueBox", "markers", "scales", "guides", "preview", "zooming", "lens"],
		bar       : ["general", "bar"      , "title", "tooltips", "legend", "valueBox",            "scales", "guides", "preview", "zooming", "lens"],
		bar3d     : ["general", "bar3d"    , "title", "tooltips", "legend", "valueBox",            "scales", "guides", "preview", "zooming", "lens"],
		hbar      : ["general", "bar"      , "title", "tooltips", "legend", "valueBox",            "scales", "guides", "preview", "zooming", "lens"],
		hbar3d    : ["general", "bar3d"    , "title", "tooltips", "legend", "valueBox",            "scales", "guides", "preview", "zooming", "lens"],
		line      : ["general", "line"     , "title", "tooltips", "legend", "valueBox", "markers", "scales", "guides", "preview", "zooming", "lens"],
		line3d    : ["general", "line3d"   , "title", "tooltips", "legend", "valueBox", "markers", "scales", "guides", "preview", "zooming", "lens"],
		pie       : ["general", "pie"      , "title", "tooltips", "legend", "valueBox",                                                            ],
        pie3d     : ["general", "pie3d"    , "title", "tooltips", "legend", "valueBox",                                                            ],
        nestedpie : ["general", "nestedpie", "title", "tooltips", "legend", "valueBox",                                                            ],
        hfunnel   : ["general", "funnel"   , "title", "tooltips", "legend", "valueBox",            "scales",                                       ],
        hfunnel3d : ["general", "funnel3d" , "title", "tooltips", "legend", "valueBox",            "scales",                                       ],
		vfunnel   : ["general", "funnel"   , "title", "tooltips", "legend", "valueBox",            "scales"                                        ],
		vfunnel3d : ["general", "funnel3d" , "title", "tooltips", "legend", "valueBox",            "scales",                                       ],
		vbullet   : ["general", "bullet"   , "title", "tooltips", "legend", "valueBox",            "scales", "guides", "preview", "zooming", "lens"],
		hbullet   : ["general", "bullet"   , "title", "tooltips", "legend", "valueBox",            "scales", "guides", "preview", "zooming", "lens"],
		bubble    : ["general"             , "title", "tooltips", "legend", "valueBox",            "scales", "guides", "preview", "zooming", "lens"],
		gauge     : ["general"             , "title", "tooltips", "legend", "valueBox",            "scales",                                       ],
		piano     : ["general", "piano"    , "title", "tooltips", "legend", "valueBox",            "scales"                                        ],
		radar     : ["general", "radar"    , "title", "tooltips", "legend", "valueBox",            "scales"                                        ],
		scatter   : ["general"             , "title", "tooltips", "legend", "valueBox",            "scales", "guides", "preview", "zooming", "lens"],
		stock     : ["general"             , "title", "tooltips", "legend", "valueBox",            "scales", "guides", "preview", "zooming", "lens"],
		venn      : ["general"             , "title", "tooltips", "legend", "valueBox",                                                            ]
	},
	
	// =========================================================================
    // These will be displayed as tabs at the bottom pane
    // =========================================================================
	bottomPanePanels : {
		exportChart : {
			url  : "export.html",
			label: "Export"
		},
		data : {
			url   : "data.html",
			label : "DATA"
		},
		json : {
			url   : "json.html",
			label : "JSON"
		}
	},
    
    // =========================================================================
    // The re-usable editors and their configuration
    // =========================================================================
    reusableEditors : {
		text : {
			path : "text.html",
			event: "textSettingsChange",
			properties : [
				"text-align",
				"font-size",
				"color",
				"font-family",
				"underline",
				"bold",
				"italic"
			]
		},
		background : {
			path : "background.html",
			event: "backgroundSettingsChange",
			properties : [
				"background-image",
				"background-repeat",
				"background-transparent",
				"background-position",
				"background-fit",
				"background-color",
				"background-color-2",
				"alpha",
				"fill-type",
				"fill-angle",
				"fill-offset-x",
				"fill-offset-y"
			]
		},
		shadow : {
			path : "shadow.html",
			event: "shadowSettingsChange",
			properties : [
				"shadow-color",
				"shadow-alpha",
				"shadow-distance",
				"shadow-blur-x",
				"shadow-blur-y",
				"shadow"
			]
		},
		line : {
			path : "line.html",
			event: "lineSettingsChange",
			properties : [
				"line-width",
				"line-color",
				"line-dashed",
				"line-dotted",
				"line-segment-size",
				"line-gap-size",
				"alpha"
			]
		},
		bevel : {
			path : "bevel.html",
			event: "bevelSettingsChange",
			properties : [
				"bevel-blur-x",
				"bevel-blur-y",
				"bevel-distance",
				"bevel-angle",
				"bevel"
			]
		},
		dimensions : {
			path : "dimensions.html",
			event: "dimensionsSettingsChange",
			properties : [
				"width", 
				"height"
			]
		},
		margins : {
			path : "margins.html",
			event: "marginSettingsChange",
			properties : [
				"margin-top", 
				"margin-right", 
				"margin-bottom", 
				"margin-left"
			]
		},
		position : {
			path : "position.html",
			event: "positionSettingsChange",
			properties : [
				"position"
			]
		},
		positionXY : {
			path : "position_xy.html",
			event: "positionXYSettingsChange",
			properties : [
				"x",
				"y"
			]
		},
		box : {
			path : "box.html",
			event: "boxSettingsChange",
			properties : [
				"width", 
				"height",
				"margin-top", 
				"margin-right", 
				"margin-bottom", 
				"margin-left",
				"position"
			]
		},
		border : {
			path : "border.html",
			event: "borderSettingsChange",
			properties : [
				"border-width", 
				"border-color"
			]
		}
	},
    
	disableSelectors : {
		".plotarea .shadow"         : FLASH,
		".plot     .spline"         : FLASH,
		".title    .shadow"         : FLASH,
		".title    .bevel"          : FLASH,
		".bevel"                    : FLASH,
		".shadow .blur"             : FLASH,
		".lens"                     : FLASH,
		".spline"                   : FLASH
	},
	
    // =========================================================================
    // Chart Type Templates
    // These are used when switching between different chart types
    // =========================================================================
    "chrtTypeTemplates" : { 
		
		"line" : {
		    "graphset" : [
		        {
		            "type" : "line",
		            "series" : [
		                {
		                    "values":[-98,95,65,78,-35,83,14,-45,-28,73],
		                    "text":"Item 0"
		                },
		                {
		                    "values":[32,-1,75,-85,-65,96,-55,-39,-57,42],
		                    "text":"Item 1"
		                },
		                {
		                    "values":[72,-41,0,-36,-83,81,99,31,94,22],
		                    "text":"Item 2"
		                },
		                {
		                    "values":[63,51,-75,-59,35,-15,33,36,-87,66],
		                    "text":"Item 3"
		                }
		            ]
		        }
		    ]
		},
		"area3d" : {
			graphset : [{
				type : "area3d",
				series : [{
				    values : [11, 26, 7, 44, 11, 28, 42, 26, 13, 32, 12, 4, 21, 11, 43, 39]
				}]
			}]
		},
		"bar" : {
		    graphset : [{
		        type : "bar",
		        series : [{
		            values : [11, 26, 7, 44, 11, 28, 42, 26, 13, 32, 12, 4, 21, 11, 43, 39]
		        }]
		    }]
		},
		"bar3d" : {
		    graphset : [{
		        type : "bar3d",
		        series : [{
		            values : [11, 26, 7, 44, 11, 28, 42, 26, 13, 32, 12, 4, 21, 11, 43, 39]
		        }]
		    }]
		},
		"hbar3d" : {
		    graphset : [{
		        type : "hbar3d",
		        series : [{
		            values : [11, 26, 7, 44, 11, 28, 42, 26, 13, 32, 12, 4, 21, 11, 43, 39]
		        }]
		    }]
		},
		"hbar" : {
		    graphset : [{
		        type : "hbar",
		        series : [{
		            values : [11, 26, 7, 44, 11, 28, 42, 26, 13, 32, 12, 4, 21, 11, 43, 39]
		        }]
		    }]
		},  
		"hfunnel3d" : {
            "graphset" : [{
                "type" : "hfunnel3d",
                "series" : [{
                    "values" : [11, 26, 7, 44, 11]
                }, {
                    "values" : [44, 11, 28, 42]
                }, {
                    "values" : [26, 13, 32, 12]
                }, {
                    "values" : [21, 11, 43]
                }, {
                    "values" : [11, 43, 39]
                }]
            }]
        },
        "line3d" : {
            graphset : [{
                type : "line3d",
                series : [{
                    values : [11, 26, 7, 44, 11, 28, 42, 26, 13, 32, 12, 4, 21, 11, 43, 39]
                }]
            }]
        },
        "pie3d" : {
            "graphset" : [{
                "type" : "pie3d",
                "series" : [{
                    "text" : "Apple<br/>Maple",
                    "values" : [742.8],
                    "hover-state" : {
                        "background-color" : "#000"
                    }
                }, {
                    "text" : "Pear<br/>Bear",
                    "values" : [773.1],
                    "hover-state" : {
                        "background-color" : "#fc0",
                        "border-width" : 1,
                        "border-color" : "#fff"
                    }
                }, {
                    "text" : "Cherry<br/>Berry",
                    "values" : [550.4]
                }, {
                    "text" : "Mango<br/>Tango",
                    "values" : [618.1]
                }, {
                    "text" : "Papaya<br/>Biscaya",
                    "values" : [780.8]
                }]
            }]
        },
        "vfunnel3d" : {
            "graphset" : [{
                "type" : "vfunnel3d",
                "series" : [{
                    "values" : [6543]
                }, {
                    "values" : [5423]
                }, {
                    "values" : [3212]
                }, {
                    "values" : [2322]
                }, {
                    "values" : [1000]
                }]
            }]
        },
        "area" : {
            graphset : [{
                type : "area",
                series : [{
                    values : [11, 26, 7, 44, 11, 28, 42, 26, 13, 32, 12, 4, 21, 11, 43, 39]
                }]
            }]
        },
        "bubble" : {
            graphset : [{
                type : "bubble",
                series : [{
                    values : [[1, 15, 4], [2, 4, 2], [5, 10, 1], [6, 7, 3], [3, 6, 2], [7, 15, 1], [8, 2, 4], [1, 7, 3], [2, 12, 3], [4, 4, 4], [5, 1, 2], [6, 3, 1], [8, 16, 2]]
                }]
            }]
        },
        "gauge" : {
            graphset : [{
                type : "gauge",
                series : [{
                    "values" : [15]
                }, {
                    "values" : [88]
                }, {
                    "values" : [31]
                }, {
                    "values" : [75]
                }]
            }]
        },
        "hbullet" : {
            "graphset" : [{
                "type" : "hbullet",
                "series" : [{
                    "values" : [96, 87, 12, 92, 93],
                    "goals" : [98, 75, 25, 80, 90]
                }]
            }]
        },
        "hfunnel" : {
            "graphset" : [{
                "type" : "hfunnel",
                "series" : [{
                    "values" : [6543]
                }, {
                    "values" : [5423]
                }, {
                    "values" : [3212]
                }, {
                    "values" : [2322]
                }, {
                    "values" : [1000]
                }]
            }]
        },
        "nestedpie" : {
            "graphset" : [{
                "type" : "nestedpie",
                "series" : [{
                    "text" : "Apples",
                    "values" : [5, 15, 6, 18, 20]
                }, {
                    "text" : "Oranges",
                    "values" : [8, 3, 17, 7, 19]
                }, {
                    "text" : "Bananas",
                    "values" : [22, 22, 5, 15, 10]
                }, {
                    "text" : "Grapes",
                    "values" : [16, 8, 18, 9, 21]
                }]
            }]
        },
        "piano" : {
            "graphset" : [{
                "type" : "piano",
                "series" : [
                    {
                        "values":[44,42,9,23,83,56,52,7,28,61],
                        "text":"Item 0"
                    },
                    {
                        "values":[48,67,39,96,8,45,22,1,49,39],
                        "text":"Item 1"
                    },
                    {
                        "values":[45,72,45,96,24,44,58,92,19,88],
                        "text":"Item 2"
                    },
                    {
                        "values":[29,72,89,51,80,65,22,54,40,67],
                        "text":"Item 3"
                    },
                    {
                        "values":[14,42,28,78,35,44,4,9,5,46],
                        "text":"Item 4"
                    },
                    {
                        "values":[32,15,18,95,12,89,16,17,75,53],
                        "text":"Item 5"
                    }
                ]
            }]
        },
        "pie" : {
            "graphset" : [{
                "type" : "pie",
                "series" : [{
                    "text" : "Apple<br/>Maple",
                    "values" : [742.8],
                    "hover-state" : {
                        "background-color" : "#000"
                    }
                }, {
                    "text" : "Pear<br/>Bear",
                    "values" : [773.1],
                    "hover-state" : {
                        "background-color" : "#fc0",
                        "border-width" : 1,
                        "border-color" : "#fff"
                    }
                }, {
                    "text" : "Cherry<br/>Berry",
                    "values" : [550.4]
                }, {
                    "text" : "Mango<br/>Tango",
                    "values" : [618.1]
                }, {
                    "text" : "Papaya<br/>Biscaya",
                    "values" : [780.8]
                }]
            }]
        },
        "radar" : {
            graphset : [{
                type : "radar",
                series : [{
                    values : [11, 26, 7, 44, 11, 28, 42, 26, 13, 32, 12, 4, 21, 11, 43, 39]
                }]
            }]
        },
        "scatter" : {
            graphset : [{
                type : "scatter",
                series : [{
                    values : [[1, 15], [2, 4], [5, 10], [6, 7], [3, 6], [7, 15], [8, 2], [1, 7], [2, 12], [4, 4], [5, 0], [6, 3], [8, 16]]
                }]
            }]
        },
        "stock" : {
            graphset : [{
                type : "stock",
                series : [{
                    values : [[41, 45, 31, 35], [30, 44, 26, 28], [45, 50, 31, 40], [56, 57, 48, 49], [53, 56, 42, 50]]
                }]
            }]
        },
		"venn" : {
            "graphset" : [{
                "type" : "venn",
                "series" : [{
                    "values" : [100],
                    "join" : [5]
                }, {
                    "values" : [70],
                    "join" : [15]
                }, {
                    "values" : [90],
                    "join" : [30]
                }]
            }]
        },
        "vbullet" : {
            "graphset" : [{
                "type" : "vbullet",
                "series" : [{
                    "values" : [98, 87, 12, 92, 93],
                    "goals" : [90, 75, 25, 80, 90]
                }]
            }]
        },
        "vfunnel" : {
            "graphset" : [{
                "type" : "vfunnel",
                "series" : [{
                    "values" : [6543]
                }, {
                    "values" : [5423]
                }, {
                    "values" : [3212]
                }, {
                    "values" : [2322]
                }, {
                    "values" : [1000]
                }]
            }]
        }
    }
});
