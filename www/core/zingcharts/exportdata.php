<?php
// core/zingcharts/exportdata.php

//echo '<pre>$_POST: ' . htmlspecialchars(print_r($_POST, 1)) . '</pre>';

$data = empty($_POST['data']) ? null : $_POST['data'];
$mime = empty($_POST['mime']) ? null : $_POST['mime'];
$name = empty($_POST['name']) ? null : $_POST['name'];

if ( $mime !== null && $data !== null && $name !== null ) {
	
	
	
	
	switch ( $mime ) {
		case 'text/json':
			$data = json_encode(json_decode(stripcslashes($data)));
			header("Content-type: $mime");
			header("Content-Disposition: attachment; filename=\"$name\"");
			header("Content-length: " . strlen($data));
			echo $data;
		break;
		case 'image/png':
		case 'image/jpg':
		case 'image/jpeg':
		case 'image/bmp':
			$data = base64_decode($data);
			$im = imagecreatefromstring($data);
			if ($im !== false) {
				header("Content-type: $mime");
				header("Content-Disposition: attachment; filename=\"$name\"");
				header('Content-Type: image/png');
				if ( $mime == 'image/png' )
					imagepng($im);
				elseif ( $mime == 'image/bmp' ) {
					header('Content-type: ' . image_type_to_mime_type(IMAGETYPE_WBMP));
					image2wbmp($im);
				}
				else 
					imagejpeg($im);
			}
		break;
	}
	
	
	
}