"use strict";

/**
 * Class ZingchartsEngine extends ChartEngine
 */
ChartBuilder.ZingchartsEngine = ChartBuilder.ChartEngine.extend({
    
    /**
     * Stores the chart engine configuration. This will be loaded from external
     * file during initialization.
     * @type Onject
     */
    config : {},
    
    /**
     * The current chart type. Initially set to "area".
     * @type String 
     */
    chartType : "area",
    
    /**
     * The current chart renderer. Initially set to "svg".
     * @type String 
     * @private
     */
    _renderer : "svg",
    
    /**
     * The ID of the chart container
     */
    chartID : "",
    
    /**
     * This flag indicates if the chart is currently rendered
     * @type Boolean
     * @private
     */
    _isRendered : false,
    
    /**
     * This be a reference to the HTML5 version of ZingCharts
     * @type Object
     * @private
     */
    _zingchartHTML5 : null,
    
    
    /**
     * This be a reference to the Flash version of ZingCharts (loaded inside an 
     * iframe)
     * @type Object
     * @private
     */
    _zingchartFLASH : null,
    
    /**
     * The width of the canvas
     * @type Number
     * @private
     */
    _canvasWidth : 500,
    
    /**
     * The height of the canvas
     * @type Number
     * @private
     */
    _canvasHeight : 300,
    
    /**
     * The Data Generator
     * @type ZingchartsDataGenerator
     */
    dataGenerator : null,
    
	applyUIFilters : function(context) {
		
		$("body").attr("data-engine", "zingchart");
		
		switch (this._renderer) {
			case "flash":
				$("body").removeClass("render-svg render-canvas").addClass("render-flash");
			break;
			case "svg":
				$("body").removeClass("render-flash render-canvas").addClass("render-svg");
			break;
			case "canvas":
				$("body").removeClass("render-flash render-svg").addClass("render-canvas");
			break;
		}
		
		var renderersMask = {
			"svg"    : 2,
			"canvas" : 4,
			"flash"  : 8
		};
		
		var disabled = [], enabled = [], renderer = renderersMask[this._renderer], self = this;
		
		$.each(this.config.disableSelectors, function(selector, mask) {
			if (!(mask & renderer)) {
				disabled.push(selector);
			}
			else {
				enabled.push(selector);
			}
		});
		
		$(disabled.join(", ")).disable().attr(
			"title", 
			"Not supported for the current renderer ('" + this._renderer + "')"
		);
		$(enabled.join(", ")).enable();
		
		$("body").off(".applyUIFilters").on("show.applyUIFilters", function(e) {
			self.applyUIFilters(e.target);
		});
		
		renderersMask = disabled = enabled = null;
	},
	
    /**
     * Rebuilds the sidebar to show the editor available for the current 
     * chart type.
     */
    showAvailableSidebarEditors : function() {
        
        ChartBuilder.staus.set("message", "Updating the sidebar editors list");
        
        var editors = this.config.editorsForType[this.chartType], 
            base = this.config.editorsDir,
            html = "",
            editor,
            meta,
            i;

        for ( i = 0; i < editors.length; i++ ) {
            meta = this.config.editors[editors[i]];
            html += '<dt data-url="' + base + "/" + meta.url + '">' + meta.label + '</dt>';
        }
        
        $("#editors")
		.html( html ? '<dl class="keep-state">' + html + '</dl>' : "" )
		.find("dt:first").trigger("click");
		
		this.applyUIFilters();
        
        ChartBuilder.staus.set("message", "Ready");
        editors = base = html = editor = meta = i = null;
    },
    
    /**
     * Rebuilds the chart-type selector to list the chart types available 
     * for the current renderer.
     */
    showAvailableChartTypes : function() {
        
        ChartBuilder.staus.set("message", "Updating the Chart types list");
        
        // If already there - remove the old one
        $("#chart-type").closest(".row").empty().remove();
        
        var row = $(
            '<div class="row no-border">' +
            '<div class="group-label">Chart Type: </div>' +
            '<div class="control"></div>' +
            '</div>'
        ).appendTo("#engine-container");
        
        var select = $('<select id="chart-type"/>'), 
            types = $.makeMap(this.config.chartTypesForRenderer[this._renderer]),
            html = "";
        
        for ( var type in types ) {
            html += '<option value="' + type + '">' 
                + this.config.chartTypes[type]
                + '</option>';
        };
        
        select.html(html);
        
        select.change((function(engine) {
            return function() {
                engine.setChartType($(this).val());
            };
        })(this));
        
        select.appendTo($(".control", row));
        
        select.val(this.chartType);
        
        // Free the useless objects
        row = select = types = null;
        
        ChartBuilder.staus.set("message", "Ready");
    },
    
    /**
     * Set the chart type and render it
     */
    setChartType : function(type) {
            
        function singleDataLoadCallback(engine) {
            
            // Re-fetch the new data
            engine.get__data(true);
            
            // Update this property
            engine.chartType = type;
            
            // Update the sidebar panels
            engine.showAvailableSidebarEditors();
            
            // Log the change
            ChartBuilder.Logger.log("The chart type was set to %q", type);
            
            // Fire the some events
            ChartBuilder.dispatchEvent("afterChartTypeChange", {
                engine : engine,
                type   : type
            });
            
            // Update the chartType slot at the status bar
            ChartBuilder.staus.set("chartType", engine.chartType);
        }
        
        this.addEventListener("chartComplete.afterChartTypeChange", function() {
            singleDataLoadCallback(this);
        }, true);
        
        // Load the basic data template for the given chart type
        this.render(this.config.chrtTypeTemplates[type]);
    },
    
    /**
     * Sets the renderer to the given type.
     * @param {String} one of "svg", "canvas", "flash"
     */
    setRenderer : function(renderer) {
        
        if (renderer == this._renderer) {
            return;
        }
        
        if (this._isRendered) {
            zingchart.exec(this.chartID, 'destroy');
            this._isRendered = false;
            ChartBuilder.Logger.log("The chart was destroyed!");
        }
        
        /*
         * Zingchart bugfix: This listener remains bound. It is removed here
         * and re-added below if HTML5 which better then removing it for 
         * flash only because this way it can't be added more than once.
         */
        $(document).unbind("click", this._zingchartHTML5.zc_click);
        
        // Set to SVG or Canvas
        if ( renderer != "flash" ) {
            
            // Rebuild the preview area if needed
            if (!$("#" + this.chartID).length) {
                $("#preview").html('<div id="chart" class="chart"/>');
            }
            
            // Toggle the global variable!
            window.zingchart = this._zingchartHTML5;
            
            // Remove the flash version reference (if any)
            this._zingchartFLASH = null;
            
            // Re-bind this (commented above)
            $(document).bind("click", this._zingchartHTML5.zc_click);
            
            // Set the property, log and render
            this._renderer = renderer;
            ChartBuilder.Logger.log("Setting renderer to <b>%q</b>", renderer);
            this.render(this.get__data());
            
            // Re-build the available chart types selector
            this.showAvailableChartTypes();
			
			this.applyUIFilters();
            
            // Update the renderer slot at the status bar
            ChartBuilder.staus.set("renderer", renderer);
        }
        
        // Set to Flash
        else {
            
            // Create special method that will be called when the flash frame is
            // loaded
            this.notifyFlashFrameLoaded = function(win, doc) {
                
                // Save the zingchart object from the frame's window
                this._zingchartFLASH = win.zingchart;
                
                // Toggle the global variable!
                window.zingchart = this._zingchartFLASH;
                
                // Re-attach those to the new _zingchartFLASH object
                this._zingchartFLASH.modify      = this._onZingchartModify;
                this._zingchartFLASH.complete    = this._onZingchartComplete;
                this._zingchartFLASH.plot_modify = this._onZingchartPlotModify;
                
                // FIXME: This is just to hide the color pickers when 
                // clicking inside the iframe
                $("body", doc).bind("mousedown", function() {
                    $("body").trigger("mousedown");
                });
                
                // Set the property, log and render
                this._renderer = "flash";
                ChartBuilder.Logger.log("Setting renderer to '<b>flash</b>'");
                this.render(this.get__data());
                
                // Re-build the available chart types selector
                this.showAvailableChartTypes();
                
				this.applyUIFilters();
				
                // Update the renderer slot at the status bar
                ChartBuilder.staus.set("renderer", renderer);
            
                // Self-deleting method (for single use only)!
                delete this.notifyFlashFrameLoaded;
            };
            
            // create the flash frame
            $("#preview").html(
                '<iframe style="position: absolute; width: 100%; height: 100%;"' + 
                'src="zingchart_flash.html" frameborder="0" scrolling="none" ' + 
                'allowtransparency="true"></iframe>'
            );
        }
    },
    
    /**
     * Renders the chart
     * @param {Object} data The JSON data to render. If omitted the template 
     * data from the configuration will be used.
     */
    render : function(data) {
        if (this._isRendered) {
			$(document).unbind("click", this._zingchartHTML5.zc_click);
            zingchart.exec(this.chartID, 'destroy');
            this._isRendered = false;
            ChartBuilder.Logger.log("The chart was destroyed!");
        }
        
        zingchart.render({
            liburl    : 'external/zingchart/zingchart.swf',
            data      : JSON.stringify(data || this.config.chrtTypeTemplates[this.chartType]),
            width     : this._canvasWidth,
            height    : this._canvasHeight,
            container : this.chartID,
            wmode     : "transparent",
            id        : this.chartID,
            output    : this._renderer,
            preservecontainer : true,
			exportdataurl : "core/zingcharts/exportdata.php"
        });
        
        ChartBuilder.Logger.log("The chart was rendered!");
        this._isRendered = true;
    },
	
	canSet : function(name, value) {
		var method = "canSet__" + name;
		if( $.isFunction(this[method]) ) {
			return !!this[method](value);
		}
		return true;
	},
    
    /**
     * This is one of the main workers here used to set JSON values to the 
     * chart configuration object.
     * @param {string} name The property name
     * @param {any} value The value to set
     * @return {ZingchartsEngine} Returns this instance
     */
    set : function(name, value) {
        
		if ( this.canSet(name, value) ) {
			
			// The current value
			var oldValue = this.get(name);
			
			// Fire the "beforeSet" event to give listeners a chance to modify 
			// the value or even cancel the oeration
			var beforeSetEvent = new ChartBuilder.Event("beforeSet", {
				name     : name, 
				oldValue : this.get(name),
				value    : value
			});
			this.dispatchEvent(beforeSetEvent);
			if (beforeSetEvent.isStopped()) {
				beforeSetEvent = oldValue = null;
				return this;
			}
			
			// read this again if modifies by "beforeSet" listeners
			var newValue = beforeSetEvent.data.value;
			
			try {
				
				// filter un-wanted stuff
				if ( newValue && typeof newValue == "object" ) {
					newValue = $.JsonBlackList(newValue, [SKIP_THIS_PROPERTY, undefined]);
				}
				
				var set = false;
				
				// If there is a setter for this property - call it
				var method = "set__" + name;
				if( $.isFunction(this[method]) ) {
					this[method](newValue);
					set = true;
				}
				
				else {
					var _data = this.get__data();
					if ($.JSONPath(_data, name, newValue)) {
						this.set__data(_data);
						set = true;
					}
					_data = null;
				}
				
				if (set) {
					this.get__data(true); // reload cached data
					newValue = this.get(name); // read it again after set
					if (!$.areSameByValue(newValue, oldValue)) {
						this.dispatchEvent("propertyChange", {
							name         : name, 
							oldValue     : oldValue,
							value        : newValue,
							writtenValue : newValue,
							passedValue  : value
						});
					}
					this.dispatchEvent("afterSet", {
						name         : name, 
						oldValue     : oldValue,
						value        : newValue,
						writtenValue : newValue,
						passedValue  : value
					});
				}
				method = null;
				
			}
			catch (ex) {
				ChartBuilder.Logger.error(ex.message);
			}
			
			oldValue = beforeSetEvent = newValue = null;
        }
		
        return this;
    },
    
    /**
     * Tries to remove the given property
     * @param {String} name The property name which should be JSON path
     */
    unset : function(name) {
        
        this.dispatchEvent("beforeUnset", {name: name});
        
        try {
            var method = "unset__" + name;
            if( typeof this[method] == "function") {
                this[method]();
            } 
            else {
                var _data = this.get__data();
                if ($.JSONDelete(_data, name)) {
                    this.set__data(_data);
                }
                _data = null;
            }
            
            this.dispatchEvent("afterUnset", {name: name});
            
            method = null;
        }
        catch (ex) {
            ChartBuilder.Logger.error(ex.message);
        }
        return this;
    },
    
    /**
     * Returns the given property (JSON path) or undefined
     * @param {String} name The property name which should be JSON path
     */
    get : function(name) {
        var method = "get__" + name;
        if( $.isFunction(this[method]) ) {
            return this[method]();
        }
        return $.JSONPath(this.get__data(), name);
    },
    
    // =========================================================================
    // Accessors (starting with "get__" or "set__"
    // =========================================================================

    get__data : (function() {
        var _data; // Data cache
        return function(force) {
            if(force || !_data) {
				var d = zingchart.exec(this.chartID, "getdata");
				if (!d || d + "" == "undefined") {
					_data = {};
				}
				else {
					_data = JSON.parse(d);
				}
				d = null;
                this.dispatchEvent("chartDataChange", { data : _data });
            }
            return _data;
        };
    })(),

    set__data : function(data) {
        var newData = $.JsonBlackList( data, [SKIP_THIS_PROPERTY] );
        newData = $.JsonCleanupEmptyObjectProperies(newData);
		
		// Too many bugs in canvas
		if (this._renderer == "canvas") {
			this.render(newData);
		}
		else {
			zingchart.exec(this.chartID, "setdata", '{"data":' + JSON.stringify(newData) + '}');
		}
		this.get__data(true); // refresh cache
		newData = null;
        return this;
    },
    
    set__seriesData : function(data) {
        zingchart.exec(this.chartID, "setseriesdata", '{"data":' + JSON.stringify(data) + '}');
        this.get__data(true); // refresh cache
    },
    
    set__canvasSize : function(width, height) {
        if (width) {
            this._canvasWidth = Math.max($.intVal(width) , 0);
        }
        if (height) {
            this._canvasHeight = Math.max($.intVal(height), 0);
        }
        this.render(this.get__data());
    },
    
    get__canvasSize : function() {
        return {
            width : this._canvasWidth,
            height: this._canvasHeight
        };
    },
    
    get__canvasHeight : function() {
        return this._canvasHeight;
    },
    
    get__canvasWidth : function() {
        return this._canvasWidth;
    },
    
    /**
     * Loads and applies the chart engine configuration file
     * @param {String} path The path to the configuration file to load
     */
    loadConfig : function( path ) {
        var self = this;
        $.ajax({ url : path, dataType : "text" })
        .done(function(js) {
            $.evalWithEnvironment(js, { 
                applyConfig : function( config ) {
                    //console.log("config:", config);
                    self.applyConfig(config);
                }
            });
        })
        .fail(function(req, status, msg) {
            ChartBuilder.Logger.error("Error loading config file %q: %q", path, msg);
        });
    },
    
    applyConfig : function(config) {
        
        this.config = config;
            
        // Generate and append the "renderer" selector 
        // -----------------------------------------------------------------
        (function(self) {
            
            // If already there - remove the old one
            $("#renderer").closest(".row").remove();
            
            var row = $(
                '<div class="row no-border">' +
                '<div class="group-label">Renderer: </div>' +
                '<div class="control"></div>' +
                '</div>'
            ).appendTo("#engine-container");
            
            var select = $(
                '<select id="renderer">' +
                '<option value="svg">SVG</option>' +
                '<option value="canvas">Canvas</option>' +
                '<option value="flash">Flash</option>' +
                '</select>'
            );
            
            select.val(self._renderer).change(function() {
                self.setRenderer($(this).val());
            }).appendTo($(".control", row));
            
            // Free the useless objects
            row = select = null;
        })(this);
        
        // Add the engine-specific bottom panels if needed
        // ---------------------------------------------------------------------
        (function(self) {
            var bottomPanesToHave = self.config.bottomPanePanels, path, cur;
            $.each(self.config.bottomPanePanels, function(id, prefs) {
                path = self.config.editorsDir + "/" + prefs.url;
                cur = $('#bottom-pane .tabs-widget .tabs .tab[data-url="' + path + '"]');
                if (!cur.length) {
                    $('<a class="tab"/>')
                    .attr("data-url", path)
                    .text(prefs.label)
                    .prependTo("#bottom-pane .tabs-widget .tabs");
                }
            });
            $('#bottom-pane .tab:first').trigger("click");
            path = cur = null;
        })(this);
        
        // Generate and append the "Chart Type" selector 
        // -----------------------------------------------------------------
        this.showAvailableChartTypes();
        
        // Initialize the StatusBar object here
        // -----------------------------------------------------------------
        ChartBuilder.staus.purge();
        ChartBuilder.staus.prependSlot("chartType", "Chart Type", this.chartType);
        ChartBuilder.staus.prependSlot("renderer" , "Renderer", this._renderer);
        
        // Attach Listeners
        // -----------------------------------------------------------------
        
        // Log param setting
        this.addEventListener("afterSet", function(e) {
            ChartBuilder.Logger.log(
                "The property <b>%q</b> was set to %A", 
                e.data.name, 
                e.data.writtenValue
            );
        });
        
        // Attach zingchart event callbacks to the current renderer
        // -----------------------------------------------------------------
        this._zingchartHTML5.modify      = this._onZingchartModify;
        this._zingchartHTML5.complete    = this._onZingchartComplete;
        this._zingchartHTML5.plot_modify = this._onZingchartPlotModify;
        
        // RUN!
        // -----------------------------------------------------------------
        this.render();
        this.showAvailableSidebarEditors();
        ChartBuilder.staus.set("message", "Ready");
    },
    
    /**
     * @constructor
     */
    init : function(chartID) {
        
        // First initialize the parent class
        this._super(chartID);
        
        this.chartID = chartID;
        
        this._zingchartHTML5 = window.zingchart;
        
        this.dataGenerator = new ChartBuilder.ZingchartsDataGenerator(this);
        
        var self = this;
        
        /**
         * Zingcharts event callback
         * @param dataObj
         * @private
         */
        this._onZingchartModify = function(dataObj) {
            self.dispatchEvent("chartModify", dataObj);
            ChartBuilder.Logger.log("ZingChart Modified on Chart \"#%s\".", dataObj.id);
        };
        
        /**
         * Zingcharts event callback
         * @param dataObj
         * @private
         */
        this._onZingchartComplete = function(dataObj) {
            
            // ZingCharts bugfix!
            $("#" + self.chartID + "-img").unbind("click", zingchart.zc_click);
            if (self._renderer != "flash") {
                $("#" + self.chartID + "-img").bind("click", zingchart.zc_click);
            }
            
            // Update the _data object after each chart rendering
            self.get__data(true);
            
            self.dispatchEvent("chartComplete", dataObj);
            ChartBuilder.Logger.log("Data loaded on Chart \"#%s\".", dataObj.id);
        };
        
        /**
         * Zingcharts event callback
         * @param dataObj
         * @private
         */
        this._onZingchartPlotModify = function(plot) {
            self.dispatchEvent("plotModify", plot);
            ChartBuilder.Logger.log("Plot Modified - Plot: %i", plot.plotindex);
        };
        
        this.loadConfig("core/zingcharts/config.js");    
    },
    
    /**
     * Embeds a shared editor to the specified container.
     * @param {Object} cfg
     *   editorID
     *   target        : "body",
     *   eventNS       : "",
     *   onChange      : null,
     *   onLoad        : null,
     *   initialData   : null,
     *   initArguments : []
     */
    embedEditor : function(cfg) {
        
        var DATA = {};
        
        if ( cfg.initialData ) {
            $.extend(DATA, cfg.initialData);
        }
        
        var meta = this.config.reusableEditors[cfg.editorID];
        var path = this.config.reusableEditorsDir + "/" + meta.path;
        var evt  = meta.event + (cfg.eventNS ? "." + cfg.eventNS : "");
        
        $.loadHTML(cfg.target, path).done(function() {
            
            $(cfg.target).unbind(evt).bind(evt, function(e, data) {
                if ( e.target == $(cfg.target)[0] ) {
                    e.stopPropagation();
                    if ( $.extendByTemplate(data, DATA, meta.properties) ) {
                        if ($.isFunction(cfg.onChange)) {//console.log("_changed", data, DATA)
                            cfg.onChange(DATA, meta.properties);
                        }
                    }
                }
            });
            
            if ( cfg.initArguments ) {
                $(cfg.target).triggerHandler("initUI", cfg.initArguments);
            }
            
            if ($.isFunction(cfg.onLoad)) {
                cfg.onLoad();
            }
        });
    }
}, "ZingchartsEngine");

/**
 * Class ZingchartsDataGenerator
 */
ChartBuilder.ZingchartsDataGenerator = Class.extend({
    
    /**
     * The constructor
     * @param {ZingchartsEngine} engine That must be the ZingchartsEngine
     * instance that this generator will be attached to.
     */
    init : function( engine ) 
    {
        var minValue = 0, 
            maxValue = 100,
            numPlots = 1,
            numTicks = 10;
        
        this.engine = engine;
        
        this.setMinValue = function(min) {
            minValue = $.floatVal(min);
            return this;
        };
        
        this.getMinValue = function() {
            return minValue;
        };
        
        this.setMaxValue = function(max) {
            maxValue = $.floatVal(max);
            return this;
        };
        
        this.getMaxValue = function() {
            return maxValue;
        };
        
        this.setNimPlots = function(num) {
            numPlots = Math.max($.intVal(num), 0);
            return this;
        };
        
        this.getNimPlots = function() {
            return numPlots;
        };
        
        this.setNimTicks = function(num) {
            numTicks = Math.max($.intVal(num), 0);
            return this;
        };
        
        this.getNimTicks = function() {
            return numTicks;
        };
    },
    
    getMaxValueFromChart : function() {
        var o = this.engine.get("graphset[0].series"), 
            l = o.length, 
            plotValues, 
            x = -Infinity;
        
        for ( var i = 0; i < l; i++ ) {
            plotValues = o[i].values;
            var j, l2 = plotValues.length;
            for ( j = 0; j < l2; j++ ) {
                switch (this.engine.chartType) {
                    case "stock":
                        x = Math.max(plotValues[j][0], x);
                    break;
                    case "scatter":
                    case "bubble":
                        x = Math.max(plotValues[j][1], x);
                    break;
                    default:
                        x = Math.max(plotValues[j], x);
                    break;
                }
            }
            
            // for bubble charts
            if (o[i].goals) {
                var goals = o[i].goals, l3 = goals.length;
                for ( j = 0; j < l3; j++ ) {
                    x = Math.max(goals[j], x);
                }
            }
        }
        return x;
    },
    
    getMinValueFromChart : function() {
        var o = this.engine.get("graphset[0].series"), 
            l = o.length, 
            plotValues, 
            x = Infinity;
        
        for ( var i = 0; i < l; i++ ) {
            plotValues = o[i].values;
            var j, l2 = plotValues.length;
            for ( j = 0; j < l2; j++ ) {
                switch (this.engine.chartType) {
                    case "stock":
                        x = Math.min(plotValues[j][0], x);
                    break;
                    case "scatter":
                    case "bubble":
                        x = Math.min(plotValues[j][1], x);
                    break;
                    default:
                        x = Math.min(plotValues[j], x);
                    break;
                }
            }
            
            // for bubble charts
            if (o[i].goals) {
                var goals = o[i].goals, l3 = goals.length;
                for ( j = 0; j < l3; j++ ) {
                    x = Math.min(goals[j], x);
                }
            }
        }
        return x;
    },
    
    generate : function() {
        
        var data  = [];
        var plots = this.getNimPlots();
        var type  = this.engine.chartType;
        
        function createRandomRange(length, min, max) {
            var out = [], range = max - min;
            for ( var i = 0; i < length; i++ ) {
                out[i] = max - Math.floor(range * Math.random());
            }
            return out;
        }
        
        function createRandomScatterRange(length, min, max) {
            var out = [], range = max - min;
            for ( var i = 0; i < length; i++ ) {
                out[i] = [
                    Math.floor(length * Math.random()),
                    max - Math.floor(range * Math.random())
                ];
            }
            return out;
        }
        
        function createRandomBubbleRange(length, min, max) {
            var out = [], range = max - min;
            for ( var i = 0; i < length; i++ ) {
                out[i] = [
                    Math.floor(length * Math.random()),
                    max - Math.floor(range * Math.random()),
                    1 + Math.floor(range * 0.3 * Math.random())
                ];
            }
            return out;
        }
        
        function createRandomStockRange(length, min, max) {
            var out = [], range = max - min, point;
            for ( var i = 0; i < length; i++ ) {
                point  = Number((max - (range * Math.random())).toFixed(2));
                out[i] = [
                    point,
                    Number((point * (1 + 0.5 * Math.random())).toFixed(2)),
                    Number((point * 0.5).toFixed(2)),
                    Number((point * (1 - 0.5 * Math.random())).toFixed(2))
                ];
            }
            return out;
        }
        
        
        if (plots > 0) {
            var ticks = this.getNimTicks();
            if (ticks > 0) {
                var b1   = this.getMinValue();
                var b2   = this.getMaxValue();
                var max  = Math.max(b1, b2);
                var min  = Math.min(b1, b2);
                
                for ( var plotIndex = 0; plotIndex < plots; plotIndex++ ) {
                    switch (type) {
                        case "stock":
                            data.push({ 
                                values: createRandomStockRange(ticks, min, max), 
                                text: "item " + plotIndex 
                            });
                        break;
                        case "scatter":
                            data.push({ 
                                values: createRandomScatterRange(ticks, min, max), 
                                text: "item " + plotIndex 
                            });
                        break;
                        case "bubble":
                            data.push({ 
                                values: createRandomBubbleRange(ticks, min, max), 
                                text: "item " + plotIndex 
                            });
                        break;
                        case "hbullet":
                        case "vbullet":
                            var values =  createRandomRange(ticks, min, max);
                            var minGoal = max;
                            var maxGoal = min;
                            $.each(values, function(i, val) {
                                minGoal = Math.min(minGoal, val);
                                maxGoal = Math.max(maxGoal, val);
                            });
                            data.push({ 
                                values: values,
                                goals : createRandomRange(ticks, minGoal, max),
                                text  : "item " + plotIndex 
                            });
                        break;
                        default:
                            data.push({ 
                                values: createRandomRange(ticks, min, max), 
                                text: "item " + plotIndex 
                            });
                        break;
                    }
                }
            }
        }
        return data;
    }

}, "ZingchartsDataGenerator");