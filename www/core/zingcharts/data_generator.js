"use strict";

/**
 * Class ZingchartsDataGenerator
 */
ChartBuilder.ZingchartsDataGenerator = Class.extend({
    
    /**
     * The constructor
     * @param {ZingchartsEngine} engine That must be the ZingchartsEngine
     * instance that this generator will be attached to.
     */
    init : function( engine ) 
    {
        var minValue = 0, 
            maxValue = 100,
            numPlots = 1,
            numTicks = 10;
        
        this.engine = engine;
        
        this.setMinValue = function(min) {
            minValue = $.floatVal(min);
            return this;
        };
        
        this.getMinValue = function() {
            return minValue;
        };
        
        this.setMaxValue = function(max) {
            maxValue = $.floatVal(max);
            return this;
        };
        
        this.getMaxValue = function() {
            return maxValue;
        };
        
        this.setNimPlots = function(num) {
            numPlots = Math.max($.intVal(num), 0);
            return this;
        };
        
        this.getNimPlots = function() {
            return numPlots;
        };
        
        this.setNimTicks = function(num) {
            numTicks = Math.max($.intVal(num), 0);
            return this;
        };
        
        this.getNimTicks = function() {
            return numTicks;
        };
    },
    
    generate : function() {
        var data  = [];
        var plots = this.getNimPlots();
        if (plots > 0) {
            var ticks = this.getNimTicks();
            if (ticks > 0) {
                var b1   = this.getMinValue();
                var b2   = this.getMaxValue();
                var max  = Math.max(b1, b2);
                var min  = Math.min(b1, b2);
                var plot, tick, range = max - min;
                
                for ( var plotIndex = 0; plotIndex < plots; plotIndex++ ) {
                    plot = { values: [], text: "item " + plotIndex };
                    for ( var tickIndex = 0; tickIndex < ticks; tickIndex++ ) {
                        tick = max - Math.floor(range * Math.random());
                        plot.values.push(tick);
                    }
                    data.push(plot);
                }
            }
        }
        return data;
    }

}, "ZingchartsDataGenerator");